
# DEFINITIONS

# Set compiler, flags and libs
CXX = g++
CXXFLAGS = -Wall -pedantic -Wno-long-long -O0 -ggdb
LIBS = -lncurses

# Add custom flags
CXXFLAGS += -Wextra -std=c++14 #-Werror

# File namings
BINARY = gracijir
DOCDIR = doc

# Add magic flags
CXXFLAGS += -MD -MP

# LOAD FILES

# Get .cpp files
SOURCES = $(wildcard src/*.cpp)

# Get .o files
OBJECTS = $(SOURCES:.cpp=.o)

# Get .d files
DEFINITIONS = $(SOURCES:%.cpp=%.d)

# COMMANDS

# Default make actions - compile in this case
all: compile doc

# Compile list of binaries (just one in my case ... thanks god)
compile: $(BINARY)

# Print line counts
lines:
	@printf "Empty lines:"; cat src/*pp | grep -E "^[ ^I\n]*$$" | wc -l;\
	printf "   Comments:"; cat src/*pp | grep -E "(.*(/\*|\*/|//).*|^[ ^I]*\*.*)" | wc -l;\
	printf "       Code:"; cat src/*pp | grep -vE "(.*(/\*|\*/|//).*|^[ ^I]*\*.*|^[ ^I\n]*$$)" | wc -l;\
	printf "      TOTAL:"; cat src/*pp | wc -l

# Delete files and folders
clean:
	rm -rf $(BINARY) $(OBJECTS) $(DEFINITIONS) $(DOCDIR)
clean-doc:
	rm -rf $(DOCDIR)

# Run the target
run:
	./$(BINARY)

# Run the target with valgrind
simple-valgrind:
	valgrind ./$(BINARY)

# Run the target with valgrind
valgrind:
	@valgrind --leak-check=full --show-leak-kinds=all --log-file=valgrind.log ./$(BINARY); less valgrind.log; rm valgrind.log

# Create interactive documentation
doc:
	doxygen Doxyfile

# Create interactive documentation and check for undocumented stuff
doc-dev: clean-doc
	@doxygen Doxyfile &> doc.log;  grep "is not documented" doc.log | wc -l | less

$(BINARY): $(SOURCES:%.cpp=%.o)
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LIBS)

-include $(DEFINITIONS)
