//
// Created by Jiří Gracík on 08/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_IMOVABLE_HPP
#define PROGTEST_17_18_SEMESTRAL_IMOVABLE_HPP


class IMovable {
public:
    /**
     * Move left
     */
    virtual void MoveLeft() = 0;

    /**
     * Move right
     */
    virtual void MoveRight() = 0;

    /**
     * Jump or climb up
     */
    virtual void MoveUp() = 0;

    /**
     * Climb down
     */
    virtual void MoveDown() = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_IMOVABLE_HPP
