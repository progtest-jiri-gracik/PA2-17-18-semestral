//
// Created by Jiří Gracík on 15/05/2018.
//

#include "ChunkSizeReader.hpp"

ChunkSizeReader::ChunkSizeReader(std::ifstream& fileStream) : FileReader(fileStream) {}

Size ChunkSizeReader::GetChunkSize() {

    // Skip the chunk size comment
    m_fileStream.seekg(14);

    // Prepare buffer for numbers
    std::string            buffer(4, ' ');
    std::string::size_type sz;

    int chunkWidth, chunkHeight;

    // Read chunk width
    m_fileStream.read(&buffer[0], buffer.size());
    chunkWidth = std::stoi(buffer, &sz);

    // Read chunk height
    m_fileStream.read(&buffer[0], buffer.size());
    chunkHeight = std::stoi(buffer, &sz);

    return  {chunkWidth, chunkHeight};
}
