//
// Created by Jiří Gracík on 18/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_TREEBLOCK_HPP
#define PROGTEST_17_18_SEMESTRAL_TREEBLOCK_HPP

#include "TransparentBlock.hpp"

class TreeBlock : public TransparentBlock {

public:
    explicit TreeBlock(int data, const Point &position);

    void UpdateMomentum(int &momentum) override;

    std::string ToString() const override;

    void Update(ChunkManager &manager) override;

};

#endif //PROGTEST_17_18_SEMESTRAL_TREEBLOCK_HPP
