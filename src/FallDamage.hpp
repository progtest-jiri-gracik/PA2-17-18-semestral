//
// Created by Jiří Gracík on 12/06/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_FALLDAMAGE_HPP
#define PROGTEST_17_18_SEMESTRAL_FALLDAMAGE_HPP


#include "SimpleDamage.hpp"

class FallDamage : SimpleDamage {
public:
    explicit FallDamage(int damage) : SimpleDamage(damage) {}
};


#endif //PROGTEST_17_18_SEMESTRAL_FALLDAMAGE_HPP
