//
// Created by Jiří Gracík on 15/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_CHUNKSIZEREADER_HPP
#define PROGTEST_17_18_SEMESTRAL_CHUNKSIZEREADER_HPP

#include <fstream>
#include "Size.hpp"
#include "FileReader.hpp"

class ChunkSizeReader: public FileReader{

public:
    explicit ChunkSizeReader(std::ifstream& fileStream);
    Size GetChunkSize();
};


#endif //PROGTEST_17_18_SEMESTRAL_CHUNKSIZEREADER_HPP
