//
// Created by Jiří Gracík on 08/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_BLOCK_HPP
#define PROGTEST_17_18_SEMESTRAL_BLOCK_HPP

#include <ostream>
#include "IDrawable.hpp"
#include "ChunkManager.hpp"

/**
 * Basic class representing a generic block
 */

class ChunkManager;

class Block : public IDrawable {
public:
    Block(uint32_t character, int data, const Point &position, Color color);

    /**
     * Perform block logic (update fluids, grow trees, ...)
     */
    virtual void Update(ChunkManager &) {}

    /**
     * Draw block on its respective place
     * @param driver
     */
    void Draw(IScreenDriver &driver) override;

    /**
     * Is it a solid block (can't be walked through)
     * @return
     */
    bool IsSolid() { return m_isSolid; }

    /**
     * Is it a solid block?
     * @return
     */
    bool IsBreakable() { return m_isBreakable; }

    /**
     * Can you climb on it?
     * @return
     */
    bool IsClimbable() { return m_isClimbable; }

    /**
     * Get block position
     * @return
     */
    const Point &GetPosition() const { return m_position; }

    /**
     * Get data value
     * @return
     */
    int GetData() const {
        return m_data;
    }

    /**
     * Get block name
     * @return
     */
    virtual std::string ToString() const = 0;

    friend std::ostream &operator<<(std::ostream &os, const Block &block);

protected:

    /**
     * Set solidity
     * @param isSolid
     */
    void SetSolid(bool isSolid) { m_isSolid = isSolid; }

    /**
     * Set the ability to break
     * @param isBreakable
     */
    void SetBreakable(bool isBreakable) { m_isBreakable = isBreakable; }

    /**
     * Set the ability to break
     * @param isBreakable
     */
    void SetClimbable(bool isClimbable) { m_isClimbable = isClimbable; }

    uint32_t m_character;
    int      m_data;
    Point    m_position;
    Color    m_color;
    bool     m_isSolid     = true;
    bool     m_isBreakable = false;
    bool     m_isClimbable = false;
};


#endif //PROGTEST_17_18_SEMESTRAL_BLOCK_HPP
