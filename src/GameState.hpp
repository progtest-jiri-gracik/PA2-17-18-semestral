//
// Created by Jiří Gracík on 08/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_GAMESTATE_HPP
#define PROGTEST_17_18_SEMESTRAL_GAMESTATE_HPP


enum class GameState {
    Starting, Running, Paused, TooSmallToRun, Exiting
};


#endif //PROGTEST_17_18_SEMESTRAL_GAMESTATE_HPP
