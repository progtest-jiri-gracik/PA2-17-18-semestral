//
// Created by Jiří Gracík on 08/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_COLOR_HPP
#define PROGTEST_17_18_SEMESTRAL_COLOR_HPP


enum class Color {
    WhiteBlack = 1, RedBlack, GreenYellow, BlueWhite, CyanMagenta, WhiteMagenta, MagentaWhite, GreenWhite
};


#endif //PROGTEST_17_18_SEMESTRAL_COLOR_HPP
