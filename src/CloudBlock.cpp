//
// Created by Jiří Gracík on 14/05/2018.
//

#include <ncurses.h>
#include "CloudBlock.hpp"

CloudBlock::CloudBlock(int data, const Point &position)
        : TransparentBlock(
                97 | A_ALTCHARSET, data, position, Color::BlueWhite
        ) {}
