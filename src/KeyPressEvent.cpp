//
// Created by Jiří Gracík on 07/04/2018.
//

#include "KeyPressEvent.hpp"

int KeyPressEvent::getKey() const {
    return m_key;
}

std::string KeyPressEvent::Print() const {
    return "Keypress event for key " + std::string(m_key, 1);
}
