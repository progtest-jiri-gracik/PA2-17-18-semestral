//
// Created by Jiří Gracík on 18/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_WATERBLOCK_HPP
#define PROGTEST_17_18_SEMESTRAL_WATERBLOCK_HPP


#include "Block.hpp"

class WaterBlock : public Block {
    WaterBlock(int data, const Point &position);

    std::string ToString() const override { return "Water"; }
};


#endif //PROGTEST_17_18_SEMESTRAL_WATERBLOCK_HPP
