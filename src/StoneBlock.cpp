//
// Created by Jiří Gracík on 15/05/2018.
//

#include "StoneBlock.hpp"

StoneBlock::StoneBlock(int data, const Point &position)
        : Block(' ', data, position, Color::CyanMagenta) {}
