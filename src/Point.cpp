//
// Created by Jiří Gracík on 13/05/2018.
//

#include "Point.hpp"

Point::Point() : m_x(0), m_y(0) {}

Point::Point(std::pair<int, int> pair) : m_x(pair.first), m_y(pair.second) {}

Point::Point(int x, int y) : m_x(x), m_y(y) {}

Point &Point::operator+=(const Point &point) {
    m_x += point.m_x;
    m_y += point.m_y;
    return *this;
}

Point &Point::operator-=(const Point &point) {
    return *this += -point;
}

Point Point::operator+(const Point &point) const {
    return {m_x + point.m_x, m_y + point.m_y};
}

Point Point::operator-(const Point &point) const {
    return *this + -point;
}

Point Point::operator*(const Point &point) const {
    return {m_x * point.m_x, m_y * point.m_y};
}

Point Point::operator/(const Point &point) const {
    return {m_x / point.m_x, m_y / point.m_y};
}

Point Point::operator-() const {
    return {-m_x, -m_y};
}

Point Point::operator*(int a) const {
    return {m_x * a, m_y * a};
}

Point Point::operator/(int a) const {
    return {m_x / a, m_y / a};
}

bool operator==(const Point &lhs, const Point &rhs) {
    return lhs.m_x == rhs.m_x &&
           lhs.m_y == rhs.m_y;
}

bool operator!=(const Point &lhs, const Point &rhs) {
    return !(rhs == lhs);
}

bool operator<(const Point &lhs, const Point &rhs) {
    return lhs.m_x < rhs.m_x && lhs.m_y < rhs.m_y;
}

bool operator>(const Point &lhs, const Point &rhs) {
    return lhs.m_x > rhs.m_x && lhs.m_y > rhs.m_y;
}

bool operator<=(const Point &lhs, const Point &rhs) {
    return lhs.m_x < rhs.m_x + 1 && lhs.m_y < rhs.m_y + 1;
}

bool operator>=(const Point &lhs, const Point &rhs) {
    return lhs.m_x + 1 > rhs.m_x && lhs.m_y + 1 > rhs.m_y;
}

std::ostream &operator<<(std::ostream &os, const Point &point) {
    return os << "[" << point.m_x << "," << point.m_y << "]";
}
