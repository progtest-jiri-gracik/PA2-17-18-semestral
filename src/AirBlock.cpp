//
// Created by Jiří Gracík on 14/05/2018.
//

#include "AirBlock.hpp"

AirBlock::AirBlock(int data, const Point &position)
        : TransparentBlock(' ', data, position, Color::BlueWhite) {}

void AirBlock::UpdateMomentum(int &momentum) {
    momentum = std::min(momentum + 1, 4);
}
