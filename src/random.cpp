//
// Created by Jiří Gracík on 18/05/2018.
//

#include <algorithm>
#include <cstdlib>
#include "random.hpp"

int random(int min, int max) {
    if (min > max) {
        std::swap(min, max);
    }
    return min + std::rand() % ((max + 1) - min);
}
