//
// Created by Jiří Gracík on 22/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_INAMEABLE_HPP
#define PROGTEST_17_18_SEMESTRAL_INAMEABLE_HPP


#include <string>

class INameable {
public:
    virtual const std::string &GetName() const = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_INAMEABLE_HPP
