//
// Created by Jiří Gracík on 25/04/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_KEY_HPP
#define PROGTEST_17_18_SEMESTRAL_KEY_HPP

#include "ncurses.h"

enum class Key {
    ArrowUp    = KEY_UP,
    ArrowDown  = KEY_DOWN,
    ArrowLeft  = KEY_LEFT,
    ArrowRight = KEY_RIGHT,
    A          = 'A'
};


#endif //PROGTEST_17_18_SEMESTRAL_KEY_HPP
