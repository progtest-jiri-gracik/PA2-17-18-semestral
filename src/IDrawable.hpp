//
// Created by Jiří Gracík on 08/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_IDRAWABLE_HPP
#define PROGTEST_17_18_SEMESTRAL_IDRAWABLE_HPP


#include <utility>
#include <memory>
#include "IScreenDriver.hpp"
#include "Point.hpp"

class IScreenDriver;

class IDrawable {
public:

    /**
     * Draw the object on screen
     */
    virtual void Draw(IScreenDriver &screen) = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_IDRAWABLE_HPP
