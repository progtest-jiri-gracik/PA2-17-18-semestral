//
// Created by Jiří Gracík on 17/05/2018.
//

#include <iostream>
#include <random>
#include <iomanip>
#include "ChunkGenerator.hpp"
#include "AirBlock.hpp"
#include "ChunkEnricher.hpp"
#include "CloudBlock.hpp"
#include "DiamondBlock.hpp"
#include "Direction.hpp"
#include "DirtBlock.hpp"
#include "MetalBlock.hpp"
#include "StoneBlock.hpp"
#include "TreeBlock.hpp"
#include "TreeGenerator.hpp"
#include "random.hpp"

ChunkGenerator::ChunkGenerator(const Size &chunkSize) : m_chunkSize(chunkSize) {
    // Create seed
    std::srand(std::time(nullptr));
}

std::shared_ptr<ChunkManager> ChunkGenerator::Generate(int chunkCount) {

    std::vector<chunk_t> chunks;
    chunks.reserve(static_cast<unsigned long>(chunkCount));

    // Fill map with stone
    for (auto chunkIndex = 0; chunkIndex < chunkCount; ++chunkIndex) {
        chunks.emplace_back(static_cast<unsigned long>(m_chunkSize.GetArea()));
    }

    // Create chunk manager to manage created chunks
    auto chunkManager = std::make_shared<ChunkManager>(m_chunkSize, chunks);

    auto mapHeight = m_chunkSize.GetHeight();
    auto mapWidth  = chunkCount * m_chunkSize.GetWidth();

    auto terrainMin = static_cast<int>(m_minTerrainHeight * mapHeight);
    auto terrainMax = static_cast<int>(m_maxTerrainHeight * mapHeight);

    // Generate random terrain
    auto levels = std::vector<int>(static_cast<unsigned long>(mapWidth), -1);
    auto steps  = m_randomStepSize;

    // Generate random terrain levels with [steps] gaps
    for (auto x = 0; x < mapWidth; x += steps) {
        levels[x] = random(terrainMin, terrainMax);
    }

    // Fill in the gaps with random values similar to it's neighbours
    while (steps != 1) {
        steps /= 2;
        for (auto x = steps; x < mapWidth; x += steps * 2) {

            auto min = std::max(
                    0,
                    levels[x - steps]
            );

            auto max = x + steps < mapWidth
                       ? levels[x + steps]
                       : random(terrainMax, terrainMax);

            if (max < min) {
                std::swap(min, max);
            }

            levels[x] = random(
                    min - (steps / 4),
                    max + (steps / 4)
            );
        }
    }

    // Cover terrain with dirt and replace everything above with air
    for (auto x = 0; x < mapWidth; ++x) {

        auto level = levels[x];

        for (auto y = 0; y < level; ++y) {
            chunkManager->SetBlock(new AirBlock(0, Point(x, y)));
        }
        chunkManager->SetBlock(new DirtBlock(1, Point(x, level)));

        auto lowestDirtY = random(level + 2, level + 6);

        for (auto y = lowestDirtY; y > level; --y) {
            chunkManager->SetBlock(new DirtBlock(0, Point(x, y)));
        }
        for (auto y = lowestDirtY + 1; y < m_chunkSize.GetHeight(); y++) {
            chunkManager->SetBlock(new StoneBlock(0, Point(x, y)));
        }
    }

    // Generate metal
    ChunkEnricher(m_maxMetalLevel, m_minMetalLevel,
                  m_minMetalCountPerChunk, m_maxMetalCountPerChunk,
                  m_minMetalCountPerVein, m_maxMetalCountPerVein,
                  [](const Point &position) { return new MetalBlock(0, position); }
    ).Enrich(*chunkManager);

    // Generate diamonds
    ChunkEnricher(m_maxDiamondLevel, m_minDiamondLevel,
                  m_minDiamondCountPerChunk, m_maxDiamondCountPerChunk,
                  m_minDiamondCountPerVein, m_maxDiamondCountPerVein,
                  [](const Point &position) { return new DiamondBlock(0, position); }
    ).Enrich(*chunkManager);

    // Generate trees
    TreeGenerator(m_minForestChance, m_maxForestChance, 15, 30, 5, 10, 1, 4, 5, 15, 4, 10)
            .GenerateTrees(*chunkManager, levels);
    
    return chunkManager;
}
