//
// Created by Jiří Gracík on 07/04/2018.
//

#include <string>
#include <iostream>
#include <ncurses.h>
#include "Keyboard.hpp"

void Keyboard::Update(GameState &state) {

    // Do not update if game is not running
    if (state != GameState::Running) { return; }

    int key;

    while ((key = getch()) != ERR) {
        std::string stringKey;
        if ('a' <= key && key <= 'z') {
            key = toupper(key);
        }
        if ('A' <= key && key <= 'Z') {
            // TODO?
        }

        GetKeyPressEventEmitter(key).Emmit();
    }
}

IEventEmitter& Keyboard::GetKeyPressEventEmitter(int key) {
    auto emitterIterator = m_eventEmitters.find(key);
    if (emitterIterator == m_eventEmitters.end()) {
        m_eventEmitters.insert({key, std::make_shared<EventEmitter>([key]() {
            return new KeyPressEvent(key);
        })});
        emitterIterator = m_eventEmitters.find(key);
    }

    return *emitterIterator->second;
}
