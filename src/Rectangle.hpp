//
// Created by Jiří Gracík on 13/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_RECTANGLE_HPP
#define PROGTEST_17_18_SEMESTRAL_RECTANGLE_HPP


#include <ostream>
#include "Point.hpp"
#include "Size.hpp"

class Rectangle {
public:
    Rectangle();

    Rectangle(const Point &position, const Size &size);

    Rectangle(int x, int y, int width, int height);

    friend Rectangle operator+(const Point &position, const Size &size);

    const Point &GetPosition() const;

    const Size &GetSize() const;

    friend std::ostream &operator<<(std::ostream &os, const Rectangle &rectangle);

private:
    Point m_position;
    Size m_size;
};


#endif //PROGTEST_17_18_SEMESTRAL_RECTANGLE_HPP
