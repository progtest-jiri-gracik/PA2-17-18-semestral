//
// Created by Jiří Gracík on 08/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_IDRAWER_HPP
#define PROGTEST_17_18_SEMESTRAL_IDRAWER_HPP


#include "GameState.hpp"
#include "IDrawable.hpp"

class IDrawer {
public:
    virtual void Draw(GameState& gameState) = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_IDRAWER_HPP
