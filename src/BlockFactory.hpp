//
// Created by Jiří Gracík on 14/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_BLOCKFACTORY_HPP
#define PROGTEST_17_18_SEMESTRAL_BLOCKFACTORY_HPP

#include <map>
#include <functional>
#include "Block.hpp"

class BlockFactory {
public:
    Block* CreateBlock(char key, int data, const Point& position);

private:
    static const std::map<char, std::function<Block *(int data, const Point &position)>> m_blockGenerators;
};


#endif //PROGTEST_17_18_SEMESTRAL_BLOCKFACTORY_HPP
