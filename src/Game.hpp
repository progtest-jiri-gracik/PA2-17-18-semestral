//
// Created by Jiří Gracík on 04/04/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_GAME_HPP
#define PROGTEST_17_18_SEMESTRAL_GAME_HPP


#include "Player.hpp"
#include "Keyboard.hpp"
#include "IKeyboardDriver.hpp"
#include "IScreenDriver.hpp"
#include "IDrawer.hpp"
#include "ChunkManager.hpp"

/**
 * Game class maintains the life cycle of the game
 */
class Game : public IEventSubscriber<IKeyboardDriver> {
public:
    Game();

    void Load();

    /**
     * Set the game driver that will handle keyboard input
     * @param keyboard
     */
    void SetKeyboard(std::shared_ptr<IKeyboardDriver> keyboard);

    /**
     * Set the screen driver handling all the rendering stuff
     * @param screen
     */
    void SetScreen(std::shared_ptr<IScreenDriver> keyboard);

    /**
     * Start the game
     */
    void Run();

private:

    /**
     * Load game from file
     * @param file
     * @return
     */
    std::shared_ptr<ChunkManager> LoadFromFile(std::string file);

    /**
     * Generate new map
     * @return
     */
    std::shared_ptr<ChunkManager> Generate(const Size &chunkSize, int chunkCount);

    /**
     * Perform one game cycle composed of update and draw
     * @see Game::Draw()
     * @see Game::Update()
     */
    void Loop();

    /**
     * Perform any game logic
     */
    void Update();

    /**
     * Draw all everythig on screen
     */
    void Draw();

    /**
     * End the game
     */
    void End();

    /**
     * Subscribe to keyboard keypress events (yeah, we got those!)
     */
    void CreateSubscription(IKeyboardDriver &) override;

    GameState m_state;

    std::vector<std::shared_ptr<IDrawer>>    m_drawers;
    std::vector<std::shared_ptr<IUpdatable>> m_updaters;

    std::shared_ptr<IKeyboardDriver> m_keyboard;
    std::shared_ptr<IScreenDriver>   m_screen;
    std::shared_ptr<ChunkManager>    m_chunkManager;

    std::shared_ptr<Player> m_player;
};


#endif //PROGTEST_17_18_SEMESTRAL_GAME_HPP
