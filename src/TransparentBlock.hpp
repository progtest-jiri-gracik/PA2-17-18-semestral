//
// Created by Jiří Gracík on 17/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_TRANSPARENTBLOCK_HPP
#define PROGTEST_17_18_SEMESTRAL_TRANSPARENTBLOCK_HPP

#include <functional>
#include "Block.hpp"
#include "Color.hpp"
#include "Point.hpp"

/**
 * A block an entity
 */
class TransparentBlock : public Block {
public:
    TransparentBlock(uint32_t character, int data, const Point &position, Color color);

    virtual void UpdateMomentum(int &momentum);

    void Update(ChunkManager &manager) override;
};

#endif //PROGTEST_17_18_SEMESTRAL_TRANSPARENTBLOCK_HPP
