//
// Created by Jiří Gracík on 16/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_CHUNKMANAGER_HPP
#define PROGTEST_17_18_SEMESTRAL_CHUNKMANAGER_HPP


#include <vector>
#include "Point.hpp"
#include "IUpdatable.hpp"
#include "Block.hpp"

class Block;

typedef std::vector<std::unique_ptr<Block>> chunk_t;

class ChunkManager : public IDrawable, public IUpdatable {
public:

    explicit ChunkManager(const Size &chunkSize, std::vector<chunk_t> &chunks);

    void Update(GameState &state) override;

    void Draw(IScreenDriver &screen) override;

    /**
     * Get number of chunks
     * @return
     */
    int GetChunkCount() const { return static_cast<int>(m_chunks.size()); }

    /**
     * Get size of managed chunks
     * @return
     */
    const Size &GetChunkSize() const { return m_chunkSize; }

    /**
     * Get total size of managed chunks
     * @return
     */
    const Size &GetMapSize() const { return m_mapSize; }

    /**
     * Get a block
     * @param position
     * @return
     *
     * @see ChunkManager::operator[]
     */
    Block *GetBlock(const Point &position);

    /**
     * Is there a block with this position?
     * @param position
     * @return
     */
    bool IsValidPosition(const Point &position) const;

    /**
     * Sets or replaces a block at given position
     * @param block
     */
    void SetBlock(Block *block);

    /**
     * Another way to get a block
     * @param position
     * @return
     *
     * @see ChunkManager::GetBlock
     */
    Block *operator[](const Point &position);

private:

    /**
     * Compute chunk index from desired position and known chunk size
     * @param position
     * @return
     */
    int GetChunkIndex(const Point &position) const;

    Size                 m_chunkSize;
    std::vector<chunk_t> m_chunks;
    Size                 m_mapSize;
};


#endif //PROGTEST_17_18_SEMESTRAL_CHUNKMANAGER_HPP
