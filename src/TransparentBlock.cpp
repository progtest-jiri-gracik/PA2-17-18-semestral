//
// Created by Jiří Gracík on 17/05/2018.
//

#include <utility>
#include "TransparentBlock.hpp"

TransparentBlock::TransparentBlock(
        uint32_t character,
        int data,
        const Point &position,
        Color color
)
        : Block(character, data, position, color) {

    // This is the exact opposite of solid block
    SetSolid(false);
}

void TransparentBlock::UpdateMomentum(int &momentum) {
    momentum = std::min(momentum + 1, 1);
}

void TransparentBlock::Update(ChunkManager &manager) {
    Block::Update(manager);
}
