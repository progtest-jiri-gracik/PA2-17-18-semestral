//
// Created by Jiří Gracík on 13/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_IHURTABLE_HPP
#define PROGTEST_17_18_SEMESTRAL_IHURTABLE_HPP


class IHurtable {
public:

    /**
     * Deal damage to this entity
     * @param hp
     */
    virtual void Hurt(int hp) = 0;

    /**
     * Heal this entity
     * @param hp
     */
    virtual void Heal(int hp) = 0;

    /**
     * Get current hitpoints
     * @return
     */
    virtual int GetHitpoints() const = 0;

    /**
     * Get maximum amount of hitpoints
     * @return
     */
    virtual int GetMaxHitpoints() const = 0;

    /**
     * Is the entity dead?
     * @return
     */
    virtual bool IsDead() const = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_IHURTABLE_HPP
