//
// Created by Jiří Gracík on 07/04/2018.
//

#include <cassert>
#include <utility>
#include <iostream>
#include "Player.hpp"
#include "Direction.hpp"


Player::Player(const Point &position, Color color)
        : Entity(position, ACS_DARROW, color, 100) {}


void Player::CreateSubscription(IKeyboardDriver &keyboard) {
    keyboard.GetKeyPressEventEmitter('W') += [this]() {
        MoveUp();
    };
    keyboard.GetKeyPressEventEmitter('A') += [this]() {
        MoveLeft();
    };
    keyboard.GetKeyPressEventEmitter('S') += [this]() {
        MoveDown();
    };
    keyboard.GetKeyPressEventEmitter('D') += [this]() {
        MoveRight();
    };

    keyboard.GetKeyPressEventEmitter(' ') += [this]() {
        DirectionAwareBreakBlock();
    };

    auto directionEventCallback = [this](std::shared_ptr<Event> event) {
        try {
            auto e = *std::dynamic_pointer_cast<KeyPressEvent>(event);
            SetDirection(e.getKey());
        } catch (...) {
            throw;
        }
    };

    keyboard.GetKeyPressEventEmitter('T') += directionEventCallback;
    keyboard.GetKeyPressEventEmitter('Z') += directionEventCallback;
    keyboard.GetKeyPressEventEmitter('U') += directionEventCallback;
    keyboard.GetKeyPressEventEmitter('G') += directionEventCallback;
    keyboard.GetKeyPressEventEmitter('H') += directionEventCallback;
    keyboard.GetKeyPressEventEmitter('J') += directionEventCallback;
    keyboard.GetKeyPressEventEmitter('B') += directionEventCallback;
    keyboard.GetKeyPressEventEmitter('N') += directionEventCallback;
    keyboard.GetKeyPressEventEmitter('M') += directionEventCallback;
}

void Player::Update(GameState &state) {

    bool isMovingLeft  = m_move[static_cast<int>(Direction::Left)];
    bool isMovingRight = m_move[static_cast<int>(Direction::Right)];

    if (IsDead()) {
        m_character = ACS_PLMINUS;
    } else if (isMovingLeft && !isMovingRight) {
        m_character = ACS_LEQUAL;
    } else if (isMovingRight && !isMovingLeft) {
        m_character = ACS_GEQUAL;
    } else {
        m_character = 'X';
    }
    Entity::Update(state);

    UpdateCamera();
}

void Player::Draw(IScreenDriver &screen) {
    Entity::Draw(screen);
}

void Player::SetScreen(IScreenDriver *screen) {
    m_screen = screen;
}

void Player::UpdateCamera() {
    auto screenSize = m_screen->GetScreenSize();
    auto newCamera     = m_position - (screenSize.ToPoint() / 2);

    // Clipping (show maximum one line of void blocks on any side)
    auto mapWithoutScreen = m_chunkManager->GetMapSize() - screenSize;
    auto clippedCamera    = Point(
            std::min(std::max(newCamera.GetX(), -1), mapWithoutScreen.GetWidth() + 1),
            std::min(std::max(newCamera.GetY(), -1), mapWithoutScreen.GetHeight() + 1)
    );

    // Update camera position
    m_screen->SetCamera(clippedCamera);
}


void Player::DirectionAwareBreakBlock() {
    BreakBlock(m_position + Direction::ToPoint(m_direction));
}

void Player::SetDirection(int key) {
    switch (key) {
        case 'T':
            m_direction = {Direction::TopLeft};
            break;
        case 'Z':
            m_direction = {Direction::Top};
            break;
        case 'U':
            m_direction = {Direction::TopRight};
            break;
        case 'G':
            m_direction = {Direction::Left};
            break;
        case 'H':
            m_direction = {Direction::Center};
            break;
        case 'J':
            m_direction = {Direction::Right};
            break;
        case 'B':
            m_direction = {Direction::BottomLeft};
            break;
        case 'N':
            m_direction = {Direction::Bottom};
            break;
        case 'M':
            m_direction = {Direction::BottomRight};
            break;
        default:
            assert("There should be no other key!" == nullptr);
            break;
    }
}

Direction::Directions Player::GetDirection() {
    return m_direction;
}

uint32_t Player::GetDirectionChar() {
    switch (m_direction) {
        case Direction::Left:
            return ACS_VLINE;
        case Direction::Right:
            return ACS_VLINE;
        case Direction::Top:
            return ACS_HLINE;
        case Direction::Bottom:
            return ACS_HLINE;
        case Direction::TopLeft:
            return ACS_ULCORNER;
        case Direction::TopRight:
            return ACS_URCORNER;
        case Direction::BottomLeft:
            return ACS_LLCORNER;
        case Direction::BottomRight:
            return ACS_LRCORNER;
        case Direction::Center:
            return 'o';
        default:
            assert("Unknown direction!");
            return 0;
    }
}

Block *Player::GetSelectedBlock() {
    return m_chunkManager->GetBlock(m_position + Direction::ToPoint(m_direction));
}

Block *Player::GetBreakedBlock() {
    return m_chunkManager->GetBlock(m_lastBreakedBlock);
}
