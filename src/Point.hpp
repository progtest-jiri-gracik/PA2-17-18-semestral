//
// Created by Jiří Gracík on 13/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_POINT_HPP
#define PROGTEST_17_18_SEMESTRAL_POINT_HPP


#include <utility>
#include <ostream>

class Point {
public:

    /**
     * Construct an empty point {0, 0}
     */
    Point();

    /**
     * Construct a point from pair of x and y
     * @param pair
     */
    explicit Point(std::pair<int, int> pair);

    /**
     * Construct a point from x and y
     * @param x
     * @param y
     */
    Point(int x, int y);

    /**
     * Get x-axis value
     * @return
     */
    int GetX() const { return m_x; }

    /**
     * Get y-axis value
     * @return
     */
    int GetY() const { return m_y; }

    Point &operator+=(const Point &point);

    Point &operator-=(const Point &point);

    Point operator+(const Point &point) const;

    Point operator-(const Point &point) const;

    Point operator*(const Point &point) const;

    Point operator/(const Point &point) const;

    Point operator-() const;

    Point operator*(int a) const;

    Point operator/(int a) const;

    friend bool operator==(const Point &lhs, const Point &rhs);

    friend bool operator!=(const Point &lhs, const Point &rhs);

    friend bool operator<(const Point &lhs, const Point &rhs);

    friend bool operator>(const Point &lhs, const Point &rhs);

    friend bool operator<=(const Point &lhs, const Point &rhs);

    friend bool operator>=(const Point &lhs, const Point &rhs);

    friend std::ostream &operator<<(std::ostream &os, const Point &point);

protected:
    int m_x;
    int m_y;
};


#endif //PROGTEST_17_18_SEMESTRAL_POINT_HPP
