//
// Created by Jiří Gracík on 18/05/2018.
//

#include <cassert>
#include <cstdlib>
#include "Direction.hpp"

Direction::Directions Direction::GetRandomDirection() {
    return static_cast<Directions >(std::rand() % 8);
}

Point Direction::ToPoint(Direction::Directions direction) {
    switch (direction) {

        case Left:
            return {-1, 0};
        case Right:
            return {1, 0};
        case Top:
            return {0, -1};
        case Bottom:
            return {0, 1};
        case TopLeft:
            return ToPoint(Top) + ToPoint(Left);
        case TopRight:
            return ToPoint(Top) + ToPoint(Right);
        case BottomLeft:
            return ToPoint(Bottom) + ToPoint(Left);
        case BottomRight:
            return ToPoint(Bottom) + ToPoint(Right);
        case Center:
            return {};
        default:
            assert("Bad direction" == nullptr);
            return {};
    }
}
