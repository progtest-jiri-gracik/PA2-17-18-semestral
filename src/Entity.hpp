//
// Created by Jiří Gracík on 08/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_ENTITY_HPP
#define PROGTEST_17_18_SEMESTRAL_ENTITY_HPP


#include <array>
#include "IDrawable.hpp"
#include "IMovable.hpp"
#include "IHurtable.hpp"
#include "IUpdatable.hpp"
#include "ChunkManager.hpp"

class Entity : public IDrawable, public IMovable, public IHurtable, public IUpdatable {
public:
    Entity(const Point &position, uint32_t character, Color color, int maxHp);

    /**
     * Get current on-map position
     * @return
     */
    Point GetPosition();

    void Update(GameState &state) override;

    void Draw(IScreenDriver &screen) override;

    void MoveLeft() override;

    void MoveRight() override;

    void MoveUp() override;

    void MoveDown() override;

    /**
     * Break block at given position
     * @param position
     */
    void BreakBlock(const Point &position);

    void Hurt(int hp) override;

    void Heal(int hp) override;

    int GetHitpoints() const override { return m_hp; }

    int GetMaxHitpoints() const override { return m_maxHp; }

    bool IsDead() const override;

    double GetBreakingProgress() const;

    /**
     * Did the entity get hurt in this tick?
     * @return
     */
    bool GotHurt() { return m_gotHurt; }

    /**
     * Is this entity breaking a block?
     * @return
     */
    bool IsBreaking() { return m_wasBreaking; }

    /**
     * Is this entity breaking a breakable block?
     * @return
     */
    bool IsTargetBlockBreakable() { return m_isTargetBlockBreakable; }

    /**
     * Is this entity breaking a breakable block?
     * @return
     */
    bool CanBreakTargetBlockBlock() { return m_isTargetBlockBreakable && m_breakTargetHardness <= m_breakingPower; }

    /**
     * Set chunk manager for the ability to move and interact with the map
     * @param chunkManager
     */
    void SetChunkManager(ChunkManager *chunkManager);

protected:

    bool MoveHorizontally(bool moveLeft);

    // Visual
    uint32_t m_character;
    Color    m_color;

    // Positional
    Point               m_position;
    std::array<bool, 4> m_move;
    int                 m_momentum = 0;

    // Hurtable
    int  m_maxHp;
    int  m_hp;
    bool m_gotHurt;

    // Logic
    ChunkManager *m_chunkManager;

    // Mining
    Point m_breakedBlock,
          m_lastBreakedBlock;
    int   m_breakingProgress       = 0,
          m_breakingPower          = 1,
          m_breakTargetHardness    = 0,
          m_breakTargetMiningTime  = 0;
    bool  m_isBreaking             = false,
          m_wasBreaking            = false,
          m_isTargetBlockBreakable;
};


#endif //PROGTEST_17_18_SEMESTRAL_ENTITY_HPP
