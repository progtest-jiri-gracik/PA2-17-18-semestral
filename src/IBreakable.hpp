//
// Created by Jiří Gracík on 18/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_IBREAKABLE_HPP
#define PROGTEST_17_18_SEMESTRAL_IBREAKABLE_HPP


#include <vector>
#include <list>
#include "Item.hpp"

class IBreakable {
public:
    virtual std::list<std::unique_ptr<Item>> DropItems() const = 0;

    virtual int GetHardness() const = 0;

    virtual int GetMiningTime() const = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_IBREAKABLE_HPP
