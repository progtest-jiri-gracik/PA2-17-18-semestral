//
// Created by Jiří Gracík on 08/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_EVENTEMITTER_HPP
#define PROGTEST_17_18_SEMESTRAL_EVENTEMITTER_HPP

#include <functional>
#include <memory>
#include <vector>
#include "IEventEmitter.hpp"
#include "Event.hpp"

class EventEmitter : public IEventEmitter {
public:

    /**
     * Create new event emitter object with specified event generating function
     * @param eventGenerator
     */
    explicit EventEmitter(std::function<Event *()> eventGenerator);

    /**
     * Subscribe to event and require the event object and event emitter for each callback call
     * @param callback
     */
    void Subscribe(const std::function<void(std::shared_ptr<Event> event)> &callback) override;

    /**
     * Simply subscribe to event without additional information
     * @param callback
     */
    void Subscribe(const std::function<void()> &callback) override;

    /**
     * A nice way to subscribe
     * @param callback
     */
    void operator+=(const std::function<void(std::shared_ptr<Event> event)> &callback) override;

    /**
     * A nice way to subscribe
     * @param callback
     * @see
     */
    void operator+=(const std::function<void()> &callback) override;

    /**
     * Fires event to all subscribed callbacks
     */
    void Emmit() override;

protected:
    std::function<Event *()>                                       m_eventGenerator;
    std::vector<std::function<void(std::shared_ptr<Event> event)>> m_callbacks;
    std::vector<std::function<void()>>                             m_simple_callbacks;
};


#endif //PROGTEST_17_18_SEMESTRAL_EVENTEMITTER_HPP
