//
// Created by Jiří Gracík on 13/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_IDAMAGER_HPP
#define PROGTEST_17_18_SEMESTRAL_IDAMAGER_HPP


class IDamager {
    int GetDamage();
};


#endif //PROGTEST_17_18_SEMESTRAL_IDAMAGER_HPP
