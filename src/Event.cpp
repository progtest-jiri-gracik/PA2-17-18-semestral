//
// Created by Jiří Gracík on 07/04/2018.
//

#include "Event.hpp"

std::string Event::Print() const {
    return "Event";
}

std::ostream &operator<<(std::ostream &os, const Event &event) {
    return os << event.Print();
}
