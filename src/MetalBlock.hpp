//
// Created by Jiří Gracík on 14/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_METALBLOCK_HPP
#define PROGTEST_17_18_SEMESTRAL_METALBLOCK_HPP

#include "Block.hpp"

class MetalBlock : public Block {
public:
    explicit MetalBlock(int data, const Point &position);

    std::string ToString() const override { return "Metal"; }
};


#endif //PROGTEST_17_18_SEMESTRAL_METALBLOCK_HPP
