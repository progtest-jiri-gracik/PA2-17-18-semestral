//
// Created by Jiří Gracík on 18/05/2018.
//

#include "BreakableBlock.hpp"
#include "random.hpp"

BreakableBlock::BreakableBlock(int hardness, int miningTime) : m_hardness(hardness), m_miningTime(miningTime) {}

int BreakableBlock::GetHardness() const {
    return m_hardness;
}

int BreakableBlock::GetMiningTime() const {
    return m_miningTime;
}
