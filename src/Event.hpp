//
// Created by Jiří Gracík on 07/04/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_EVENT_HPP
#define PROGTEST_17_18_SEMESTRAL_EVENT_HPP


#include <string>
#include <ostream>

class Event {
public:
    virtual std::string Print() const;

public:
    friend std::ostream &operator<<(std::ostream &os, const Event &event);
};


#endif //PROGTEST_17_18_SEMESTRAL_EVENT_HPP
