//
// Created by Jiří Gracík on 18/05/2018.
//

#include <ncurses.h>
#include "WaterBlock.hpp"

WaterBlock::WaterBlock(int data, const Point &position)
        : Block(97 | A_ALTCHARSET, data, position, Color::BlueWhite) {}