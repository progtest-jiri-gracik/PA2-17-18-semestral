//
// Created by Jiří Gracík on 17/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_CHUNKGENERATOR_HPP
#define PROGTEST_17_18_SEMESTRAL_CHUNKGENERATOR_HPP

#include "Size.hpp"
#include "ChunkManager.hpp"

class ChunkGenerator {
public:
    explicit ChunkGenerator(const Size &chunkSize);

    std::shared_ptr<ChunkManager> Generate(int chunkCount);

private:

    Size m_chunkSize;

    // Terrain
    constexpr static double m_maxTerrainHeight = 0.3,
                            m_minTerrainHeight = 0.4;
    static const int        m_randomStepSize   = 32;

    // Forest
    constexpr static double m_minForestChance = 0.2,
                            m_maxForestChance = 0.5;


    // Material levels
    static const int m_maxDiamondCountPerVein = 6,
                     m_minDiamondCountPerVein = 2,
                     m_maxMetalCountPerVein   = 16,
                     m_minMetalCountPerVein   = 4;

    // Material richness
    constexpr static double m_maxDiamondLevel = 0.6,
                            m_minDiamondLevel = 0.96,
                            m_maxMetalLevel   = 0.3,
                            m_minMetalLevel   = 0.8;

    // Material rareness
    constexpr static int m_maxDiamondCountPerChunk = 2,
                         m_minDiamondCountPerChunk = 0,
                         m_maxMetalCountPerChunk   = 6,
                         m_minMetalCountPerChunk   = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_CHUNKGENERATOR_HPP
