//
// Created by Jiří Gracík on 08/05/2018.
//

#include <iostream>
#include "EventEmitter.hpp"

EventEmitter::EventEmitter(std::function<Event *()> eventGenerator)
        : m_eventGenerator(std::move(eventGenerator)) {}

void EventEmitter::Subscribe(const std::function<void(std::shared_ptr<Event> event)> &callback) {
    m_callbacks.push_back(callback);
}

void EventEmitter::Subscribe(const std::function<void()> &callback) {
    m_simple_callbacks.push_back(callback);
}

void EventEmitter::operator+=(const std::function<void(std::shared_ptr<Event> event)> &callback) {
    Subscribe(callback);
}

void EventEmitter::operator+=(const std::function<void()> &callback) {
    Subscribe(callback);
}

void EventEmitter::Emmit() {
    auto event = std::shared_ptr<Event>(m_eventGenerator());

    for (const auto &callback : m_callbacks) {
        callback(event);
    }
    for (const auto &callback : m_simple_callbacks) {
        callback();
    }
}
