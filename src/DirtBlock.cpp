//
// Created by Jiří Gracík on 09/05/2018.
//

#include <ncurses.h>
#include <memory>
#include "DirtBlock.hpp"
#include "DirtItem.hpp"

DirtBlock::DirtBlock(int data, const Point &position)
        : Block(data == 0 ? ' ' : 97 | A_ALTCHARSET, data, position, Color::GreenYellow),
          BreakableBlock(1, 4) {}

std::list<std::unique_ptr<Item>> DirtBlock::DropItems() const {
    std::list<std::unique_ptr<Item>> items;

    items.emplace_back(new DirtItem(1));
    return items;
}
