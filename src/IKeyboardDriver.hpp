//
// Created by Jiří Gracík on 08/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_IKEYBOARDDRIVER_HPP
#define PROGTEST_17_18_SEMESTRAL_IKEYBOARDDRIVER_HPP


class IKeyboardDriver : public IUpdatable {
public:
    virtual IEventEmitter& GetKeyPressEventEmitter(int key) = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_IKEYBOARDDRIVER_HPP
