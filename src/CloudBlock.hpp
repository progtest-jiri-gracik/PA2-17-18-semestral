//
// Created by Jiří Gracík on 14/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_CLOUDBLOCK_HPP
#define PROGTEST_17_18_SEMESTRAL_CLOUDBLOCK_HPP

#include "TransparentBlock.hpp"

class CloudBlock : public TransparentBlock {
public:
    explicit CloudBlock(int data, const Point& position);

    std::string ToString() const override { return "Cloud"; }
};


#endif //PROGTEST_17_18_SEMESTRAL_CLOUDBLOCK_HPP
