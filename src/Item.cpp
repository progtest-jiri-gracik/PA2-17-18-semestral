//
// Created by Jiří Gracík on 18/05/2018.
//

#include "Item.hpp"

int Item::GetCount() {
    return m_count;
}

void Item::SetCount(int count) {
    m_count = count;
}
