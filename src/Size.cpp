//
// Created by Jiří Gracík on 13/05/2018.
//

#include "Size.hpp"

Size::Size() : m_point(new Point(0, 0)) {}

Size::Size(const Point &point) : m_point(new Point(point)) {}

Size::Size(std::pair<int, int> pair) : m_point(new Point(pair)) {}

Size::Size(int width, int height) : m_point(new Point(width, height)) {}

int Size::GetWidth() const {
    return m_point->GetX();
}

int Size::GetHeight() const {
    return m_point->GetY();
}

int Size::GetArea() const {
    return m_point->GetX() * m_point->GetY();
}

Point Size::ToPoint() const {
    return *m_point;
}

Size &Size::operator+=(const Size &size) {
    *m_point += *size.m_point;
    return *this;
}

Size &Size::operator-=(const Size &size) {
    return *this += -size;
}

Size Size::operator+(const Size &size) const {
    return Size(*m_point + *size.m_point);
}

Size Size::operator-(const Size &size) const {
    return *this + -size;
}

Size Size::operator-() const {
    return Size(-*m_point);
}

Size Size::operator*(int a) const {
    return Size(*m_point * a);
}

Size Size::operator/(int a) const {
    return Size(*m_point / a);
}

bool operator==(const Size &lhs, const Size &rhs) {
    return lhs.m_point == rhs.m_point;
}

bool operator!=(const Size &lhs, const Size &rhs) {
    return !(rhs == lhs);
}

std::ostream &operator<<(std::ostream &os, const Size &size) {
    return os << *size.m_point;
}

bool operator<(const Size &lhs, const Size &rhs) {
    return lhs.GetArea() < rhs.GetArea();
}

bool operator>(const Size &lhs, const Size &rhs) {
    return rhs < lhs;
}

bool operator<=(const Size &lhs, const Size &rhs) {
    return !(rhs < lhs);
}

bool operator>=(const Size &lhs, const Size &rhs) {
    return !(lhs < rhs);
}
