//
// Created by Jiří Gracík on 14/05/2018.
//

#include <ncurses.h>
#include "MetalBlock.hpp"

MetalBlock::MetalBlock(int data, const Point &position)
        : Block(104 | A_ALTCHARSET, data, position, Color::WhiteMagenta) {}
