//
// Created by Jiří Gracík on 07/04/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_KEYBOARD_HPP
#define PROGTEST_17_18_SEMESTRAL_KEYBOARD_HPP


#include <map>
#include <memory>
#include "IEventEmitter.hpp"
#include "EventEmitter.hpp"
#include "KeyPressEvent.hpp"
#include "Key.hpp"
#include "IUpdatable.hpp"
#include "GameState.hpp"
#include "IKeyboardDriver.hpp"

class Keyboard : public IKeyboardDriver {
public:
    void Update(GameState &state) override;

    IEventEmitter& GetKeyPressEventEmitter(int key) override;

private:
    std::map<int, std::shared_ptr<IEventEmitter>> m_eventEmitters;
};


#endif //PROGTEST_17_18_SEMESTRAL_KEYBOARD_HPP
