//
// Created by Jiří Gracík on 25/04/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_IEVENTEMITTER_HPP
#define PROGTEST_17_18_SEMESTRAL_IEVENTEMITTER_HPP

#include <functional>
#include "Event.hpp"

class IEventEmitter {
public:

    IEventEmitter() = default;

    virtual void Emmit() = 0;

    virtual void Subscribe(const std::function<void(std::shared_ptr<Event> event)> &callback) = 0;

    virtual void Subscribe(const std::function<void()> &callback) = 0;

    virtual void operator+=(const std::function<void(std::shared_ptr<Event> event)> &callback) = 0;

    virtual void operator+=(const std::function<void()> &callback) = 0;
};

#endif //PROGTEST_17_18_SEMESTRAL_IEVENTEMITTER_HPP
