//
// Created by Jiří Gracík on 25/04/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_IEVENTSUBSCRIBER_HPP
#define PROGTEST_17_18_SEMESTRAL_IEVENTSUBSCRIBER_HPP

template<class TEventEmitter>
class IEventSubscriber {
    
    /**
     * Subscribe to provided event emitter
     * @param emitter
     */
    virtual void CreateSubscription(TEventEmitter &emitter) = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_IEVENTSUBSCRIBER_HPP
