//
// Created by Jiří Gracík on 14/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_DIAMONDBLOCK_HPP
#define PROGTEST_17_18_SEMESTRAL_DIAMONDBLOCK_HPP

#include "Block.hpp"

class DiamondBlock : public Block {
public:
    explicit DiamondBlock(int data, const Point &position);

    std::string ToString() const override { return "Diamond"; }
};


#endif //PROGTEST_17_18_SEMESTRAL_DIAMONDBLOCK_HPP
