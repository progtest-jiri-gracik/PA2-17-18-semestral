//
// Created by Jiří Gracík on 18/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_BREAKABLEBLOCK_HPP
#define PROGTEST_17_18_SEMESTRAL_BREAKABLEBLOCK_HPP

#include <memory>
#include <functional>
#include "IBreakable.hpp"

/**
 * A block that can be broken
 */
class BreakableBlock : public IBreakable {
public:
    /**
     * Initiate breakable block with given hardness and mining time
     * @param hardness
     * @param miningTime
     */
    BreakableBlock(int hardness, int miningTime);

    int GetHardness() const override;

    int GetMiningTime() const override;

protected:
    
    /**
     * Block hardness - entity with greater or equal breaking power can break this block
     */
    int m_hardness;

    /**
     * Number of ticks to mine this block
     */
    int m_miningTime;
};


#endif //PROGTEST_17_18_SEMESTRAL_BREAKABLEBLOCK_HPP
