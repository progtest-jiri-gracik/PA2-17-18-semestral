//
// Created by Jiří Gracík on 15/05/2018.
//

#include <vector>
#include <iostream>
#include "ChunkReader.hpp"
#include "BlockFactory.hpp"

ChunkReader::ChunkReader(std::ifstream &fileStream, const Size &chunkSize)
        : FileReader(fileStream),
          m_chunkSize(chunkSize) {}

bool ChunkReader::HasChunk() {
    // Get chunk size
    // ((block count) * (block key + separator + data + whitespace = 4) - (last whitespace) + (comment with newline)
    int chunkFileSize = (m_chunkSize.GetArea() * 4 - 1 + 7);

    // Get current file pointer position
    auto currentPosition = m_fileStream.tellg();

    // Count the remaining size of file and compare it to expected chunk size
    m_fileStream.seekg(0, std::ios::end);
    bool hasChunk = m_fileStream.tellg() - currentPosition >= chunkFileSize;

    // Reset the file pointer
    m_fileStream.seekg(currentPosition, std::ios::beg);

    return hasChunk;
}

chunk_t ChunkReader::GetChunk() {

    // Prepare a factory, chunk and buffer
    BlockFactory factory;
    chunk_t      chunk;
    std::string  buffer(3, ' ');

    // Generate blocks into a chunk
    chunk.reserve(static_cast<unsigned int>(m_chunkSize.GetArea()));
    m_fileStream.seekg(6, std::ios::cur);
    for (int j = 0; j < m_chunkSize.GetArea(); j++) {
        m_fileStream.seekg(1, std::ios::cur);
        m_fileStream.read(&buffer[0], buffer.size());

        auto xOffset = m_chunkSize.GetWidth() * m_chunksRead;
        auto ptr     = factory.CreateBlock(
                buffer[0],
                buffer[2] - 48,
                {
                        xOffset + (j % m_chunkSize.GetWidth()),
                        j / m_chunkSize.GetWidth()
                }
        );
        chunk.emplace_back(ptr);
    }
    m_fileStream.seekg(1, std::ios::cur);

    m_chunksRead++;
    return chunk;
}
