//
// Created by Jiří Gracík on 15/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_CHUNKREADER_HPP
#define PROGTEST_17_18_SEMESTRAL_CHUNKREADER_HPP


#include "FileReader.hpp"
#include "Block.hpp"
#include "ChunkManager.hpp"

class ChunkReader : public FileReader {

public:
    /**
     * Create a file reader for specified file and chunk size
     * @param fileStream
     * @param chunkSize
     */
    explicit ChunkReader(std::ifstream &fileStream, const Size &chunkSize);

    /**
     * Read a chunk of defined size from provided file stream
     * @return
     */
    chunk_t GetChunk();

    /**
     * Can we read another chunk from file provided?
     * @return
     */
    bool HasChunk();

    Size m_chunkSize;
    int  m_chunksRead = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_CHUNKREADER_HPP
