//
// Created by Jiří Gracík on 14/05/2018.
//

#include <ncurses.h>
#include "DiamondBlock.hpp"

DiamondBlock::DiamondBlock(int data, const Point &position)
        : Block(96 | A_ALTCHARSET, data, position, Color::CyanMagenta) {}
