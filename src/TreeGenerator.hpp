//
// Created by Jiří Gracík on 19/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_TREEGENERATOR_HPP
#define PROGTEST_17_18_SEMESTRAL_TREEGENERATOR_HPP


#include <array>
#include "ChunkManager.hpp"

class TreeGenerator {
public:

    TreeGenerator(double minForestChance, double maxForestChance, int minTreesPerForest, int maxTreesPerForest,
                  int minPineHeight, int maxPineHeight, int minPineDistance, int maxPineDistance, int minMappleHeight,
                  int maxMappleHeight, int minMappleDistance, int maxMappleDistance);

    void GenerateTrees(ChunkManager& chunkManager, std::vector<int> levels);

protected:

    int GeneratePine(ChunkManager& chunkManager, const Point &position);

    int GenerateMapple(ChunkManager& chunkManager, const Point &position);

    double m_minForestChance, m_maxForestChance;

    int m_minTreesPerForest, m_maxTreesPerForest,
        m_minPineHeight, m_maxPineHeight, m_minPineDistance, m_maxPineDistance,
        m_minMappleHeight, m_maxMappleHeight, m_minMappleDistance, m_maxMappleDistance;

    static const std::array<int, 3> m_pineBlockData;
};


#endif //PROGTEST_17_18_SEMESTRAL_TREEGENERATOR_HPP
