//
// Created by Jiří Gracík on 15/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_STONEBLOCK_HPP
#define PROGTEST_17_18_SEMESTRAL_STONEBLOCK_HPP


#include "Block.hpp"

class StoneBlock : public Block {
public:
    explicit StoneBlock(int data, const Point &position);

    std::string ToString() const override { return "Stone"; }
};


#endif //PROGTEST_17_18_SEMESTRAL_STONEBLOCK_HPP
