//
// Created by Jiří Gracík on 12/06/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_SIMPLEDAMAGE_HPP
#define PROGTEST_17_18_SEMESTRAL_SIMPLEDAMAGE_HPP

#include "IDamage.hpp"
class SimpleDamage : IDamage {
public:
    explicit SimpleDamage(int damage) : m_damage(damage) {}

private:
    int m_damage = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_SIMPLEDAMAGE_HPP
