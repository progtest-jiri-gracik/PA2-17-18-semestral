//
// Created by Jiří Gracík on 18/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_RANDOM_HPP
#define PROGTEST_17_18_SEMESTRAL_RANDOM_HPP

int random(int min, int max);


#endif //PROGTEST_17_18_SEMESTRAL_RANDOM_HPP
