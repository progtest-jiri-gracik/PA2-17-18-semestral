//
// Created by Jiří Gracík on 11/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_SCREEN_HPP
#define PROGTEST_17_18_SEMESTRAL_SCREEN_HPP

#include <array>
#include <map>
#include "Block.hpp"
#include "IScreenDriver.hpp"
#include "Player.hpp"

/**
 * Ncurses implementation of IScreenDriver
 * @see IScreenDriver
 */
class Screen : public IScreenDriver {
public:

    /**
     * Initialize screen with specified size
     * @param size
     */
    explicit Screen(const Size &size);

    /**
     * Cleanup
     */
    virtual ~Screen();

    void Draw(GameState &gameState) override;

    /**
     * Generic object printing at specified position and color
     * @tparam T
     * @param position
     * @param object
     * @param color
     */
    template<class T>
    void Print(const Point &position, const T &object, Color color);

    /**
     * Generic object printing at current position with supplied color
     * @tparam T
     * @param object
     * @param color
     */
    template<class T>
    void Print(const T &object, Color color);

    /**
     * Generic object printing at current position with default color
     * @tparam T
     * @param object
     */
    template<class T>
    void Print(const T &object);

    void Print(const Point &position, std::string string, Color color) override;

    void Print(std::string string, Color color) override;

    void Print(std::string string) override;

    void Print(const Point &pair, uint32_t c, Color color) override;

    void Print(uint32_t c, Color color) override;

    void Print(uint32_t c) override;

    void DrawBorder(const Rectangle &rectangle, const std::string &name, Color color) override;

    void AddDrawable(std::shared_ptr<IDrawable> drawable, int depth) override;

    Point GetCursorPosition() const override;

    const Point &GetCamera() const override;

    const Size &GetScreenSize() const override;

    void SetCamera(const Point &position) override;

    void SetTerminalSize(const Size &terminalSize) override;

private:

    /**
     * Notify screen driver, that the terminal has been resized
     */
    void TerminalResized();

    /**
     * Init ncurses terminal
     * @param createResizeHandler
     */
    void InitTerminal(bool createResizeHandler = false);

    /**
     * Cleanup ncurses terminal
     * @param removeResizeHandler
     */
    void EndTerminal(bool removeResizeHandler = false);

    /**
     * Set output color
     * @param color
     */
    void SetColor(Color color) const;

    /**
     * Set ncurses pointer position
     * @param position
     */
    void Move(const Point &position);

    /**
     * Print current available controls and it's labels
     */
    void PrintControls();

    /**
     * Print health bar
     */
    void PrintPlayerHealth();

    /**
     * Print players direction
     */
    void PrintPlayerDirection();

    /**
     * Print current players action
     */
    void PrintPlayerAction();

    /**
     * Print a progressbar
     * @param position
     * @param name
     * @param borderColor
     * @param lineColor
     * @param precision
     * @param min
     * @param max
     * @param value
     */
    void PrintProgressBar(
            const Point &position, const std::string &name,
            Color borderColor, Color lineColor,
            int precision, int min, int max, int value
    );


    // Constants
    static const Color    m_defaultColor = Color::WhiteBlack;
    static const uint32_t m_border[3][3];

    // Variables
    Rectangle m_area;
    Point     m_camera;
    GameState m_gameState;

    std::multimap<int, std::shared_ptr<IDrawable>>                 m_drawables;
    std::shared_ptr<Player>                                        m_player;

    // Controls
    std::vector<std::pair<std::string, std::function<bool()>>> m_controls;

    bool m_isTooSmall      = false;
    bool m_terminalResized = false;
};

/**
 * Resize signal handler
 * @param signal
 */
void ncursesResizeHandler(int signal);


#endif //PROGTEST_17_18_SEMESTRAL_SCREEN_HPP
