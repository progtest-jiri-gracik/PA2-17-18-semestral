//
// Created by Jiří Gracík on 15/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_FILEREADER_HPP
#define PROGTEST_17_18_SEMESTRAL_FILEREADER_HPP


#include <fstream>

class FileReader {
protected:
    explicit FileReader(std::ifstream &fileStream);

    std::ifstream &m_fileStream;
};


#endif //PROGTEST_17_18_SEMESTRAL_FILEREADER_HPP
