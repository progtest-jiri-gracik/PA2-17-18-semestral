//
// Created by Jiří Gracík on 16/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_DIRECTION_HPP
#define PROGTEST_17_18_SEMESTRAL_DIRECTION_HPP

#include "Point.hpp"

class Direction {
public:
    enum Directions {
        Left, Right, Top, Bottom, TopLeft, TopRight, BottomLeft, BottomRight, Center
    };

    static Directions GetRandomDirection();

    static Point ToPoint(Directions direction);
};

#endif //PROGTEST_17_18_SEMESTRAL_DIRECTION_HPP
