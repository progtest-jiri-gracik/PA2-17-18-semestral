//
// Created by Jiří Gracík on 08/05/2018.
//

#include "Entity.hpp"

#include <utility>
#include <iostream>
#include "Direction.hpp"
#include "to_string.hpp"
#include "TransparentBlock.hpp"
#include "BreakableBlock.hpp"
#include "AirBlock.hpp"


Entity::Entity(const Point &position, uint32_t character, Color color, int maxHp)
        : m_character(character),
          m_color(color),
          m_position(position),
          m_move({{false, false, false, false}}),
          m_maxHp(maxHp),
          m_hp(maxHp) {}

Point Entity::GetPosition() {
    return m_position;
}

void Entity::Draw(IScreenDriver &screen) {
    auto topLeftCorner     = screen.GetCamera();
    auto bottomRightCorner = topLeftCorner + screen.GetScreenSize().ToPoint();

    if (topLeftCorner <= m_position && m_position < bottomRightCorner) {
        screen.Print(m_position, m_character, m_color);
    }
}

void Entity::MoveLeft() {
    m_move[static_cast<int>(Direction::Left)] = true;
}

void Entity::MoveRight() {
    m_move[static_cast<int>(Direction::Right)] = true;
}

void Entity::MoveUp() {
    m_move[static_cast<int>(Direction::Top)] = true;
}

void Entity::MoveDown() {
    m_move[static_cast<int>(Direction::Bottom)] = true;
}

void Entity::BreakBlock(const Point &position) {
    m_isBreaking   = true;
    m_breakedBlock = position;
}

void Entity::Hurt(int hp) {
    if (m_hp > 0) {
        m_hp      = std::max(m_hp - hp, 0);
        m_gotHurt = true;
    }
}

void Entity::Heal(int hp) {
    m_hp = std::max(m_hp + hp, m_maxHp);
}

bool Entity::IsDead() const {
    return m_hp <= 0;
}

double Entity::GetBreakingProgress() const {
    return m_isTargetBlockBreakable && m_breakTargetMiningTime != 0
           ? static_cast<double>(m_breakingProgress) / m_breakTargetMiningTime
           : 0.0;
}

void Entity::Update(GameState &state) {

    if (state == GameState::Running) {

        // Reset hurt indicator
        m_gotHurt          = false;

        // Block breaking
        if (m_isBreaking) {
            if (m_lastBreakedBlock == m_breakedBlock) {

                // Entity is breaking a block and it is still the same block as last time

                if (CanBreakTargetBlockBlock()) {

                    // Block can be broken and player has enough power to break it

                    // Progress
                    m_breakingProgress = std::min(m_breakTargetMiningTime, m_breakingProgress + m_breakingPower);

                    // Finished breaking
                    if (m_breakTargetMiningTime == m_breakingProgress) {
                        auto block = dynamic_cast<BreakableBlock *>(m_chunkManager->GetBlock(m_breakedBlock));

                        for (const auto &item : block->DropItems()) {
                            item->GetCount();
                        }

                        // Invalidates the old block!
                        m_chunkManager->SetBlock(new AirBlock(0, m_breakedBlock));

                        // Reset progress
                        m_isTargetBlockBreakable = false;
                    }
                }
            } else if (auto breakableBlock = dynamic_cast<BreakableBlock *>(m_chunkManager->GetBlock(m_breakedBlock))) {

                // Entity just began to break a breakable block
                m_isTargetBlockBreakable = true;
                m_breakingProgress       = 0;
                m_breakTargetMiningTime  = breakableBlock->GetMiningTime();
                m_breakTargetHardness    = breakableBlock->GetHardness();
            } else {

                // Entity is either retarded or curious what happens when it tries to break a nonbreakable block ...
                m_isTargetBlockBreakable = false;
            }
        }
        m_lastBreakedBlock = m_breakedBlock;
        m_wasBreaking      = m_isBreaking;
        m_isBreaking       = false;


        // Movement
        auto isStanding     = false;
        auto isClimbing     = (*m_chunkManager)[m_position]->IsClimbable();
        auto updateMomentum = true;

        // Stop moving if climbing
        if (isClimbing) {
            m_momentum = 0;
        }

        // Movement - horizontal
        auto wantsToMoveHorizontally = false;
        auto moveLeft                = m_move[static_cast<int>(Direction::Left)];
        auto moveRight               = m_move[static_cast<int>(Direction::Right)];

        if (!IsDead()) {
            if ((wantsToMoveHorizontally = (moveLeft != moveRight))) {
                wantsToMoveHorizontally = !MoveHorizontally(moveLeft);
            }
        }

        if (!isClimbing || IsDead()) {

            // Movement - gravity
            for (auto distance = 0; distance <= m_momentum; distance++) {

                // Try to move horizontally if it wasn't successful last time
                if (wantsToMoveHorizontally) {
                    wantsToMoveHorizontally = !MoveHorizontally(moveLeft);
                }

                auto block = (*m_chunkManager)[m_position + Point(0, 1)];
                if (block != nullptr && (block->IsSolid() || block->IsClimbable())) {
                    // TODO: Apply fall damage
                    isStanding     = true;
                    m_momentum     = 0;
                    updateMomentum = false;
                    break;
                }
                m_position += {0, 1};
            }

            // Movement - jumping
            for (auto distance = 0; m_momentum < distance; distance--) {
                auto block = (*m_chunkManager)[m_position - Point(0, 1)];
                if (block != nullptr && block->IsSolid()) {
                    // TODO: Apply head hit damage?
                    m_momentum     = 0;
                    updateMomentum = false;
                    break;
                }
                m_position -= {0, 1};
            }
        }

        // Movement - update
        if (!isStanding || !isClimbing) {
            //m_position += {0, m_momentum};
        }

        // Update momentum
        if (updateMomentum) {
            auto block = (*m_chunkManager)[m_position];
            if (block != nullptr) {
                static_cast<TransparentBlock *>(block)->UpdateMomentum(m_momentum);
            }
        }

        // Vertical movement
        auto moveUp   = m_move[static_cast<int>(Direction::Top)];
        auto moveDown = m_move[static_cast<int>(Direction::Bottom)];


        if (!IsDead()) {
            // Climb if not dead
            if (isClimbing) {

                // Climb up or down
                if (moveDown != moveUp) {
                    auto yAdd        = moveUp ? -1 : 1;
                    auto newPosition = m_position + Point(0, yAdd);

                    if (!(*m_chunkManager)[newPosition]->IsSolid()) {
                        m_position = newPosition;
                    }
                }
            } else if (moveUp && isStanding) {
                // Movement - jump
                m_momentum = -2;
            } else if (moveDown) {
                auto newPosition = m_position + Point(0, +1);
                auto block       = (*m_chunkManager)[newPosition];

                if (block->IsClimbable()) {
                    m_position = newPosition;
                }
            }
        }
    }

    // Reset movement
    m_move = {{false, false, false, false}};
}

void Entity::SetChunkManager(ChunkManager *chunkManager) {
    m_chunkManager = chunkManager;
}

bool Entity::MoveHorizontally(bool moveLeft) {

    auto newPosition = Point(
            m_position.GetX() + (moveLeft ? -1 : 1),
            m_position.GetY()
    );
    auto block       = (*m_chunkManager)[newPosition];

    if (block != nullptr && !block->IsSolid()) {
        m_position = newPosition;
        return true;
    }
    return false;
}

