//
// Created by Jiří Gracík on 07/04/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_KEYPRESSEVENT_HPP
#define PROGTEST_17_18_SEMESTRAL_KEYPRESSEVENT_HPP


#include <string>
#include "Event.hpp"

class KeyPressEvent : public Event {

public:
    explicit KeyPressEvent(int key) : m_key(key) {}

    std::string Print() const override;

    /**
     * Get pressed key
     * @return
     */
    int getKey() const;

private:
    int m_key;
};


#endif //PROGTEST_17_18_SEMESTRAL_KEYPRESSEVENT_HPP
