//
// Created by Jiří Gracík on 18/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_ITEM_HPP
#define PROGTEST_17_18_SEMESTRAL_ITEM_HPP


#include <string>
#include "ICountable.hpp"
#include "INameable.hpp"

class Item : public ICountable, public INameable {
public:
    Item() : m_count(0) {}

    explicit Item(int count) : m_count(count) {}

    /**
     * Get item count
     * @return
     */
    int GetCount() override;

    /**
     * Set new item count
     * @param count
     */
    void SetCount(int count) override;

protected:
    int               m_count;
};


#endif //PROGTEST_17_18_SEMESTRAL_ITEM_HPP
