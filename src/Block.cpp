//
// Created by Jiří Gracík on 08/05/2018.
//

#include "Block.hpp"

Block::Block(uint32_t character, int data, const Point &position, Color color)
        : m_character(character), m_data(data), m_position(position), m_color(color) {}

void Block::Draw(IScreenDriver &driver) {
    driver.Print(m_position, m_character, m_color);
}

std::ostream &operator<<(std::ostream &os, const Block &block) {
    os << block.ToString();
    return os;
}
