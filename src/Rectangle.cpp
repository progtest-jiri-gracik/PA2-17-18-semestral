//
// Created by Jiří Gracík on 13/05/2018.
//

#include "Rectangle.hpp"

Rectangle::Rectangle() = default;

Rectangle::Rectangle(const Point &position, const Size &size) : m_position(position), m_size(size) {}

Rectangle::Rectangle(int x, int y, int width, int height) : m_position(x, y), m_size(width, height) {}

Rectangle operator+(const Point &position, const Size &size) {
    return {position, size};
}

const Point &Rectangle::GetPosition() const {
    return m_position;
}

const Size &Rectangle::GetSize() const {
    return m_size;
}

std::ostream &operator<<(std::ostream &os, const Rectangle &rectangle) {
    os << "{ position: " << rectangle.m_position << ", size: " << rectangle.m_size << " }";
    return os;
}
