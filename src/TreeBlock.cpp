//
// Created by Jiří Gracík on 18/05/2018.
//

#include <cassert>
#include <ncurses.h>
#include "TreeBlock.hpp"
#include "random.hpp"

TreeBlock::TreeBlock(int data, const Point &position)
        : TransparentBlock(120 | A_ALTCHARSET, data, position, data == 0 ? Color::MagentaWhite : Color::GreenWhite) {

    switch (data) {
        case 0:
            m_character = 120 | A_ALTCHARSET; // Trunk
            SetClimbable(true); // Trunk can be climbed
            break;
        case 1:
            m_character = '/';  // Pine needles
            break;
        case 2:
            m_character = '\\';  // Pine needles
            break;
        case 3:
            m_character = 45 | A_ALTCHARSET;  // Pine top
            break;
        default:
            assert("Bad tree block data value" == nullptr);
            break;
    }
}

void TreeBlock::UpdateMomentum(int &momentum) {
    // Fall slowly through leaves and needles but let climb on trunk
    momentum = std::min(momentum + 1, m_data == 0 ? 4 : 1);
}

std::string TreeBlock::ToString() const {
    switch (m_data) {
        case 0:
            return "Wood";
        case 1:
        case 2:
            return "Pine needles";
        case 3:
            return "Pine top";
        default:
            assert("Bad tree block data value" == nullptr);
            return "Unknown";
    }
}

void TreeBlock::Update(ChunkManager &manager) {

    TransparentBlock::Update(manager);

    // Not a pine top
    if (m_data != 3) {
        return;
    }

    // Most of the times the tree won't grow
    if (random(0, 1000) > 10) {
        return;
    }

    // If there is a block above and it is not solid, we will grow
    auto newPosition = GetPosition() + Point(0, -1);
    auto newPineTop  = manager.GetBlock(newPosition);
    if (newPineTop != nullptr && !newPineTop->IsSolid()) {

        // Set new pine top
        manager.SetBlock(new TreeBlock(GetData(), newPosition));

        // This block becomes a trunk
        m_data      = 0;
        m_character = 120 | A_ALTCHARSET; // Trunk
        SetClimbable(true); // Trunk can be climbed
        m_color = Color::MagentaWhite;

        // Check for pine needles
        auto oldLeftNeedles  = dynamic_cast<TreeBlock *>(manager.GetBlock(GetPosition() + Point(-1, 1))),
             oldRightNeedles = dynamic_cast<TreeBlock *>(manager.GetBlock(GetPosition() + Point(1, 1)));
        auto newLeftNeedles  = manager.GetBlock(GetPosition() + Point(-1, 0)),
             newRightNeedles = manager.GetBlock(GetPosition() + Point(1, 0));

        // There are pine needles on the left side belonging to this tree or there is a space for new needles
        if (
                (oldLeftNeedles != nullptr && newLeftNeedles != nullptr) &&
                (
                        oldLeftNeedles->GetData() == 1 ||
                        (!newLeftNeedles->IsSolid() && dynamic_cast<TreeBlock *>(newLeftNeedles) == nullptr)
                )) {
            manager.SetBlock(new TreeBlock(1, newLeftNeedles->GetPosition()));
        }

        // There are pine needles on the right side belonging to this tree or there is a space for new needles
        if (
                (oldRightNeedles != nullptr && newRightNeedles != nullptr) &&
                (
                        oldRightNeedles->GetData() == 2 ||
                        (!newRightNeedles->IsSolid() && dynamic_cast<TreeBlock *>(newRightNeedles) == nullptr)
                )) {
            manager.SetBlock(new TreeBlock(2, newRightNeedles->GetPosition()));
        }
    }
}
