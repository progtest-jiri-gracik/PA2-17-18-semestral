//
// Created by Jiří Gracík on 08/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_IUPDATABLE_HPP
#define PROGTEST_17_18_SEMESTRAL_IUPDATABLE_HPP


#include "GameState.hpp"

class IUpdatable {
public:

    /**
     * Perform any required logic
     * @param state
     */
    virtual void Update(GameState &state) = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_IUPDATABLE_HPP
