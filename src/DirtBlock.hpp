//
// Created by Jiří Gracík on 09/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_DIRTBLOCK_HPP
#define PROGTEST_17_18_SEMESTRAL_DIRTBLOCK_HPP


#include "Block.hpp"
#include "BreakableBlock.hpp"

class DirtBlock : public Block, public BreakableBlock {
public:
    explicit DirtBlock(int data, const Point &position);

    std::string ToString() const override { return m_data == 0 ? "Dirt" : "Grass"; }

    std::list<std::unique_ptr<Item>> DropItems() const override;
};


#endif //PROGTEST_17_18_SEMESTRAL_DIRTBLOCK_HPP
