//
// Created by Jiří Gracík on 14/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_AIRBLOCK_HPP
#define PROGTEST_17_18_SEMESTRAL_AIRBLOCK_HPP

#include "TransparentBlock.hpp"

class AirBlock : public TransparentBlock {
public:
    explicit AirBlock(int data, const Point &position);

    void UpdateMomentum(int &momentum) override;

    std::string ToString() const override { return "Air"; }
};


#endif //PROGTEST_17_18_SEMESTRAL_AIRBLOCK_HPP
