//
// Created by Jiří Gracík on 19/05/2018.
//

#include <iostream>
#include <cassert>
#include "TreeGenerator.hpp"
#include "TreeBlock.hpp"
#include "random.hpp"

TreeGenerator::TreeGenerator(
        double minForestChance, double maxForestChance, int minTreesPerForest, int maxTreesPerForest,
        int minPineHeight, int maxPineHeight, int minPineDistance, int maxPineDistance,
        int minMappleHeight, int maxMappleHeight, int minMappleDistance, int maxMappleDistance
)
        : m_minForestChance(minForestChance),
          m_maxForestChance(maxForestChance),
          m_minTreesPerForest(minTreesPerForest),
          m_maxTreesPerForest(maxTreesPerForest),
          m_minPineHeight(minPineHeight), m_maxPineHeight(maxPineHeight),
          m_minPineDistance(minPineDistance),
          m_maxPineDistance(maxPineDistance),
          m_minMappleHeight(minMappleHeight),
          m_maxMappleHeight(maxMappleHeight),
          m_minMappleDistance(minMappleDistance),
          m_maxMappleDistance(maxMappleDistance) {

}

void TreeGenerator::GenerateTrees(ChunkManager &chunkManager, std::vector<int> levels) {

    std::function<int(ChunkManager &chunkManager, const Point &position)> treeGenerator;


    auto forestCount = random(
            static_cast<int>(chunkManager.GetChunkCount() * m_minForestChance),
            static_cast<int>(chunkManager.GetChunkCount() * m_maxForestChance)
    );


    for (; forestCount > 0; --forestCount) {

        auto randomGeneratorIndex = 0; //random(0, 1);
        switch (randomGeneratorIndex) {
            case 0:
                treeGenerator = [&](ChunkManager &chunkManager, const Point &position) {
                    return GeneratePine(chunkManager, position);
                };
                break;
            case 1:
                treeGenerator = [&](ChunkManager &chunkManager, const Point &position) {
                    return GenerateMapple(chunkManager, position);
                };
                break;
            default:
                assert("Random is broken" == nullptr);
                break;
        }

        int x, y;
        do {
            x = random(12, chunkManager.GetMapSize().GetWidth() - 12);
            y = levels[x] - 1;
        } while (y < 0);

        for (int treeCount = random(m_minTreesPerForest, m_maxTreesPerForest); 0 < treeCount; --treeCount) { ;
            auto newX = treeGenerator(chunkManager, {x, y});

            for (auto i = x; i < newX; ++i) {
                levels[i] = -1;
            }
            x = newX;
            y = levels[x] - 1;

            if (chunkManager.GetMapSize().GetWidth() - 5 <= x || y < 0) {
                break;
            }
        }
    }
}

int TreeGenerator::GeneratePine(ChunkManager &chunkManager, const Point &position) {

    auto trunk = position;
    chunkManager.SetBlock(new TreeBlock(0, trunk));

    // Will it overwrite other pines?
    bool willOverWrite = random(0, 1) == 0;

    // Generate pine body
    for (int remainingHeight = random(m_minPineHeight, m_maxPineHeight) - 2; 0 < remainingHeight; --remainingHeight) {

        trunk += {0, -1};

        // Left side, trunk and right side
        for (int blockIndex = 0; blockIndex < 3; blockIndex++) {
            auto blockPosition = trunk + Point(blockIndex - 1, 0);
            auto block         = chunkManager[blockPosition];

            // Will not overwrite void and solid blocks
            if (block != nullptr && !block->IsSolid()) {

                // Either overwrite trees or do not
                if (willOverWrite || dynamic_cast<TreeBlock *>(block) == nullptr) {
                    chunkManager.SetBlock(new TreeBlock(m_pineBlockData[blockIndex], blockPosition));
                }
            }
        }
    }

    // Create tree top block
    chunkManager.SetBlock(new TreeBlock(3, trunk + Point(0, -1)));

    // Return next tree position
    return position.GetX() + random(m_minPineDistance, m_maxPineDistance);
}

int TreeGenerator::GenerateMapple(ChunkManager &chunkManager, const Point &position) {
    chunkManager.SetBlock(new TreeBlock(0, position));

    return position.GetX() + random(m_minMappleDistance, m_maxMappleDistance);
}

const std::array<int, 3> TreeGenerator::m_pineBlockData = {{1, 0, 2}};
