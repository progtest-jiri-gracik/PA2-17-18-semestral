//
// Created by Jiří Gracík on 08/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_ISCREENDRIVER_HPP
#define PROGTEST_17_18_SEMESTRAL_ISCREENDRIVER_HPP

#include <utility>
#include "IDrawer.hpp"
#include "Color.hpp"
#include "Point.hpp"
#include "Size.hpp"
#include "Rectangle.hpp"

class IDrawable;

/**
 * Class representing a screen driver - something that can draw in terminal
 */
class IScreenDriver : public IDrawer {
public:

    /**
     * Print string at specified position with specified color
     * @param position
     * @param string
     * @param color
     */
    virtual void Print(const Point &position, std::string string, Color color) = 0;

    /**
     * Print string at current position with specified color
     * @param string
     * @param color
     */
    virtual void Print(std::string string, Color color) = 0;

    /**
     * Print string at current position with default color
     * @param string
     */
    virtual void Print(std::string string) = 0;

    /**
     * Print char at specified position with specified color
     * @param position
     * @param c
     * @param color
     */
    virtual void Print(const Point &position, uint32_t c, Color color) = 0;

    /**
     * Print char at current position with default color
     * @param c
     * @param color
     */
    virtual void Print(uint32_t c, Color color) = 0;

    /**
     * Print char at current position with default color
     * @param c
     */
    virtual void Print(uint32_t c) = 0;

    /**
     * Draw a border around specified area with a name in upper left corner and specified color
     * @param rectangle
     * @param name
     * @param color
     */
    virtual void DrawBorder(const Rectangle &rectangle, const std::string &name, Color color) = 0;

    /**
     * Add a thing to draw with priority (higher priority = drawn later = draws over lower priority stuff)
     * @param priority
     */
    virtual void AddDrawable(std::shared_ptr<IDrawable>, int priority) = 0;

    /**
     * Get terminal cursor position
     * @return
     */
    virtual Point GetCursorPosition() const = 0;

    /**
     * Get camera position
     * @return
     */
    virtual const Point &GetCamera() const = 0;

    /**
     * Get screen size
     * @return
     */
    virtual const Size &GetScreenSize() const = 0;

    /**
     * Set camera position
     * @param position
     */
    virtual void SetCamera(const Point &position)  = 0;

    /**
     * Sets terminal size for very cool responsive behaviour
     * @param size
     */
    virtual void SetTerminalSize(const Size &size) = 0;
};

#endif //PROGTEST_17_18_SEMESTRAL_ISCREENDRIVER_HPP
