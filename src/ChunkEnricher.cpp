//
// Created by Jiří Gracík on 18/05/2018.
//

#include "ChunkEnricher.hpp"
#include "Direction.hpp"
#include "random.hpp"


ChunkEnricher::ChunkEnricher(
        double maxLevel, double minLevel,
        int minCountPerChunk, int maxCountPerChunk,
        int minCountPerVein, int maxCountPerVein,
        const std::function<Block *(const Point &)> &blockGenerator)
        : m_maxLevel(maxLevel),
          m_minLevel(minLevel),
          m_minCountPerChunk(minCountPerChunk),
          m_maxCountPerChunk(maxCountPerChunk),
          m_minCountPerVein(minCountPerVein),
          m_maxCountPerVein(maxCountPerVein),
          m_blockGenerator(blockGenerator) {}

void ChunkEnricher::Enrich(ChunkManager &chunkManager) {

    auto chunkSize = chunkManager.GetChunkSize();
    auto mapSize   = chunkManager.GetMapSize();

    // Iterate over chunks
    for (auto chunkIndex = 0; chunkIndex < chunkManager.GetChunkCount(); ++chunkIndex) {

        auto minY = static_cast<int>(chunkSize.GetHeight() * m_maxLevel);
        auto maxY = static_cast<int>(chunkSize.GetHeight() * m_minLevel);

        // Generate veins
        for (auto chunkVeinCount = random(m_minCountPerChunk, m_maxCountPerChunk);
             chunkVeinCount > 0; --chunkVeinCount) {

            // Generate vein position
            Point position = {
                    random(chunkIndex * chunkSize.GetWidth(), (chunkIndex + 1) * chunkSize.GetWidth() - 1),
                    random(minY, maxY)
            };

            // Generate vein in case the position is valid
            if (chunkManager[position]->IsSolid()) {
                for (auto blockCount = random(m_minCountPerVein, m_maxCountPerVein); blockCount > 0; --blockCount) {

                    // Generate block and place it
                    auto block = m_blockGenerator(position);
                    chunkManager.SetBlock(block);

                    // Generate valid position for another block
                    position += Direction::ToPoint(Direction::GetRandomDirection());
                    position   = {
                            std::max(0, std::min(position.GetX(), mapSize.GetWidth() - 1)),
                            std::max(0, std::min(position.GetY(), mapSize.GetHeight() - 1))
                    };
                }
            }
        }
    }
}
