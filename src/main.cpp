
#include <iostream>
#include <ncurses.h>
#include "Game.hpp"
#include "Screen.hpp"

int main() {

    Game game;

    // Load saved map and player data
    try {
        game.Load();
    } catch (const std::ios_base::failure &e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    // Provide some peripherals
    game.SetScreen(std::make_shared<Screen>(Size(80, 24)));
    game.SetKeyboard(std::make_shared<Keyboard>());

    // Run game
    game.Run();

    return 0;
}
