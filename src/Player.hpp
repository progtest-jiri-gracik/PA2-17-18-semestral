//
// Created by Jiří Gracík on 07/04/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_PLAYER_HPP
#define PROGTEST_17_18_SEMESTRAL_PLAYER_HPP

#include "Keyboard.hpp"
#include "IEventSubscriber.hpp"
#include "EventEmitter.hpp"
#include "Entity.hpp"
#include "IKeyboardDriver.hpp"
#include "Direction.hpp"

class Player : public IEventSubscriber<IKeyboardDriver>, public Entity {
public:
    Player(const Point &position, Color color);

    void CreateSubscription(IKeyboardDriver &keyboard) override;

    void Update(GameState &state) override;

    void Draw(IScreenDriver &screen) override;

    void SetScreen(IScreenDriver *screen);

    void UpdateCamera();

    void SetDirection(int key);

    void DirectionAwareBreakBlock();

    Direction::Directions GetDirection();

    uint32_t GetDirectionChar();

    Block *GetSelectedBlock();

    Block *GetBreakedBlock();

private:
    IScreenDriver         *m_screen;
    Direction::Directions m_direction = Direction::Center;
};

#endif //PROGTEST_17_18_SEMESTRAL_PLAYER_HPP
