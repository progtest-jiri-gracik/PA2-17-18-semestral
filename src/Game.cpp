//
// Created by Jiří Gracík on 04/04/2018.
//

#include <cassert>
#include <fstream>
#include <iostream>
#include <ncurses.h>
#include <unistd.h>
#include <memory>
#include "Game.hpp"
#include "ChunkSizeReader.hpp"
#include "ChunkReader.hpp"
#include "to_string.hpp"
#include "ChunkGenerator.hpp"

Game::Game()
        : m_state(GameState::Starting),
          m_player(new Player(Point(100, 0), Color::BlueWhite)) {}

void Game::Load() {
    //m_chunkManager = LoadFromFile("examples/saves/gameA.world.sav");
    m_chunkManager = Generate({64, 128}, 10);
}

void Game::Run() {

    // Null checks
    assert(m_player != nullptr);
    assert(m_screen != nullptr);
    assert(m_keyboard != nullptr);

    // Create keyboard subscriptions
    CreateSubscription(*m_keyboard);
    m_player->CreateSubscription(*m_keyboard);

    // Assign keyboard and screen to updaters and drawers
    m_updaters.push_back(m_keyboard);
    m_drawers.push_back(m_screen);

    // Set player drawable and updatable and provide him dependencies
    m_screen->AddDrawable(m_player, 1000);
    m_screen->AddDrawable(m_chunkManager, 200);
    m_updaters.push_back(m_player);
    m_player->SetScreen(m_screen.get());
    m_player->SetChunkManager(m_chunkManager.get());
    m_updaters.push_back(m_chunkManager);

    // Game is running now
    m_state = GameState::Running;

    // Perform game loop
    while (m_state != GameState::Exiting) {
        Loop();
        usleep(1000000 / 5); // 1/5th of a second (aiming for high FPS!)
    }

    endwin();
}

std::shared_ptr<ChunkManager> Game::LoadFromFile(std::string file) {

    // Open file
    std::ifstream fileStream(file);
    if (!fileStream.good()) {
        throw std::ios_base::failure("File can't be opened. Not found or magic?");
    }

    // Load chunk size
    ChunkSizeReader chunkDimensionsReader(fileStream);
    Size            chunkSize;
    try {
        chunkSize = chunkDimensionsReader.GetChunkSize();
    } catch (...) {
        throw std::ios_base::failure("File could not be read or malformed. Is it readable and well formed?");
    }

    // Load chunks
    std::vector<chunk_t> chunks;
    ChunkReader          chunkReader(fileStream, chunkSize);
    while (chunkReader.HasChunk()) {
        try {
            chunks.push_back(chunkReader.GetChunk());
        } catch (...) {
            throw std::ios_base::failure(
                    "File could not be read or malformed - should contain " +
                    to_string(chunkSize) +
                    " block chunks, contains something else (obviously)"
            );
        }
    }

    return std::make_shared<ChunkManager>(chunkSize, chunks);
}

std::shared_ptr<ChunkManager> Game::Generate(const Size &chunkSize, int chunkCount) {
    return ChunkGenerator(chunkSize).Generate(chunkCount);
}

void Game::Loop() {

    // Update stuff
    Update();

    // Draw stuff
    Draw();
}

void Game::Update() {
    for (const auto &updater: m_updaters) {
        updater->Update(m_state);
    }
}

void Game::Draw() {
    for (const auto &drawer: m_drawers) {
        drawer->Draw(m_state);
    }
}

void Game::SetKeyboard(std::shared_ptr<IKeyboardDriver> keyboard) {
    m_keyboard = keyboard;
}

void Game::SetScreen(std::shared_ptr<IScreenDriver> screen) {
    m_screen = screen;
}

void Game::End() {
    m_state = GameState::Exiting;
}

void Game::CreateSubscription(IKeyboardDriver &keyboard) {
    keyboard.GetKeyPressEventEmitter('Q') += [this]() {
        End();
    };
}
