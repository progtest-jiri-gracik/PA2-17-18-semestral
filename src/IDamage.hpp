//
// Created by Jiří Gracík on 12/06/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_IDAMAGE_HPP
#define PROGTEST_17_18_SEMESTRAL_IDAMAGE_HPP


class IDamage {
public:
    virtual int GetDamage() = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_IDAMAGE_HPP
