//
// Created by Jiří Gracík on 19/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_DIRTITEM_HPP
#define PROGTEST_17_18_SEMESTRAL_DIRTITEM_HPP


#include "Item.hpp"

class DirtItem : public Item {
public:

    DirtItem() = default;

    explicit DirtItem(int count) : Item(count) {}

    const std::string &GetName() const override {
        return name;
    }

protected:
    static const std::string &name;
};


#endif //PROGTEST_17_18_SEMESTRAL_DIRTITEM_HPP
