//
// Created by Jiří Gracík on 16/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_TO_STRING_HPP
#define PROGTEST_17_18_SEMESTRAL_TO_STRING_HPP


#include <string>
#include <sstream>

template<class T>
std::string to_string(const T& object) {
    std::ostringstream iss;
    iss << object;
    return iss.str();
}


#endif //PROGTEST_17_18_SEMESTRAL_TO_STRING_HPP
