//
// Created by Jiří Gracík on 14/05/2018.
//

#include <iostream>
#include "BlockFactory.hpp"
#include "AirBlock.hpp"
#include "CloudBlock.hpp"
#include "DiamondBlock.hpp"
#include "DirtBlock.hpp"
#include "MetalBlock.hpp"
#include "StoneBlock.hpp"

Block *BlockFactory::CreateBlock(char key, int data, const Point &position) {

    // Search for generator
    auto blockGeneratorIterator = m_blockGenerators.find(key);

    // Generate block or return nullptr
    return (blockGeneratorIterator != m_blockGenerators.end())
           ? blockGeneratorIterator->second(data, position)
           : m_blockGenerators.find('a')->second(data, position);
}

const std::map<char, std::function<Block *(int data, const Point &position)>> BlockFactory::m_blockGenerators = {
        {'a', [](int data, const Point &position) { return new AirBlock(data, position); }},
        {'c', [](int data, const Point &position) { return new CloudBlock(data, position); }},
        {'d', [](int data, const Point &position) { return new DirtBlock(data, position); }},
        {'i', [](int data, const Point &position) { return new DiamondBlock(data, position); }},
        {'m', [](int data, const Point &position) { return new MetalBlock(data, position); }},
        {'s', [](int data, const Point &position) { return new StoneBlock(data, position); }},
};
