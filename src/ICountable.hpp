//
// Created by Jiří Gracík on 19/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_ICOUNTABLE_HPP
#define PROGTEST_17_18_SEMESTRAL_ICOUNTABLE_HPP


class ICountable {
public:
    virtual int GetCount() = 0;

    virtual void SetCount(int count) = 0;
};


#endif //PROGTEST_17_18_SEMESTRAL_ICOUNTABLE_HPP
