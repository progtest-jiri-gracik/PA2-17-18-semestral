//
// Created by Jiří Gracík on 11/05/2018.
//

#include <cassert>
#include <csignal>
#include <ncurses.h>
#include <sstream>
#include "Screen.hpp"
#include "to_string.hpp"

/**
 * Ugly lambda for ncurses resize handler
 */
std::function<void()> ncursesLambdaResizeHandler;

Screen::Screen(const Size &size)
        : m_area({}, size),
          m_camera(0, 0),
          m_gameState(GameState::Starting),
          m_controls({
                             {"[WASD] Movement",       [&]() {
                                 return m_gameState == GameState::Running && !m_player->IsDead();
                             }},
                             {"[TZUGHJBNM] Direction", [&]() {
                                 return m_gameState == GameState::Running && !m_player->IsDead();
                             }},
                             {"[Q]uit",                []() { return true; }}
                     }) {

    // Initialize ncurses
    InitTerminal(true);
}

Screen::~Screen() {

    m_controls.clear();

    // Cleanup ncurses
    EndTerminal(true);
};

void Screen::Draw(GameState &gameState) {

    if (m_terminalResized) {
        EndTerminal(false);
        InitTerminal(false);
        refresh();
        clear();
        m_terminalResized = false;
    } else {
        clear();
    }
    m_gameState = gameState;

    m_isTooSmall = m_area.GetSize().GetWidth() < 18 || m_area.GetSize().GetHeight() < 4;

    // Print border and controls
    std::string gameLabel     = (m_gameState != GameState::Paused && !m_isTooSmall) ? "Game" : "Paused";
    bool        playerGotHurt = m_player != nullptr && m_player->GotHurt();
    DrawBorder({m_camera + Point(-1, -1), m_area.GetSize()}, gameLabel,
               (!playerGotHurt || m_isTooSmall) ? m_defaultColor : Color::RedBlack);

    PrintControls();

    // Check for normal terminal size
    if (m_isTooSmall) {
        gameState = GameState::TooSmallToRun;
        Print(m_camera + m_area.GetSize().ToPoint() / 2 - Point(4, 0), "Too small", m_defaultColor);
    } else if (gameState == GameState::TooSmallToRun) {
        gameState = GameState::Running;
    }

    // Why should we draw while the game is not running?
    if (gameState != GameState::Running) {
        refresh();
        return;
    }

    // Print drawable game objects
    for (const auto &drawablePair: m_drawables) {
        auto &drawable = *drawablePair.second;

        drawable.Draw(*this);
    }

    // Print player related stuff
    if (m_player != nullptr && !m_player->IsDead()) {
        PrintPlayerHealth();
        PrintPlayerDirection();
        PrintPlayerAction();
    }

    Print(Point(3, 3) + m_camera, m_camera, m_defaultColor);

    refresh();
}

template<class T>
void Screen::Print(const Point &position, const T &object, Color color) {
    Print(position, to_string(object), color);
}

template<class T>
void Screen::Print(const T &object, Color color) {
    Print(to_string(object), color);
}

template<class T>
void Screen::Print(const T &object) {
    Print(to_string(object));
}

void Screen::Print(const Point &position, std::string string, Color color) {
    Move(position);
    Print(string, color);
}

void Screen::Print(std::string string, Color color) {
    SetColor(color);
    addstr(string.c_str());
}

void Screen::Print(std::string string) {
    Print(string, m_defaultColor);
}

void Screen::Print(const Point &position, uint32_t c, Color color) {
    Move(position);
    Print(c, color);
}

void Screen::Print(uint32_t c, Color color) {
    SetColor(color);
    addch(c);
}

void Screen::Print(uint32_t c) {
    Print(c, m_defaultColor);
}

void Screen::AddDrawable(std::shared_ptr<IDrawable> drawable, int depth) {
    m_drawables.insert({depth, drawable});

    auto player = std::dynamic_pointer_cast<Player>(drawable);
    if (player != nullptr) {
        m_player = player;
    }

}

void Screen::SetCamera(const Point &position) {
    m_camera = position;
}

void Screen::SetTerminalSize(const Size &terminalSize) {

    // Recalculate screen area
    auto size = terminalSize - Size(4, 4);
    m_area = Rectangle(
            {(terminalSize.GetWidth() - size.GetWidth()) / 2, (terminalSize.GetHeight() - size.GetHeight()) / 2},
            size
    );
}

void Screen::Move(const Point &position) {
    auto absolutePosition = m_area.GetPosition() + position - m_camera;
    move(absolutePosition.GetY(), absolutePosition.GetX());
}

void Screen::DrawBorder(const Rectangle &rectangle, const std::string &name, Color color) {
    const auto &position = rectangle.GetPosition();
    const auto &size     = rectangle.GetSize();

    SetColor(color);

    // │─
    // │
    //
    Move(position);

    hline(m_border[0][1], size.GetWidth() + 1);
    vline(m_border[1][0], size.GetHeight() + 1);

    // │─
    // │ │
    //
    Move(position + Point(size.GetWidth() + 1, 1));
    vline(m_border[1][2], size.GetHeight());

    // │─
    // │ │
    //  ─
    Move(position + Point(1, size.GetHeight() + 1));
    hline(m_border[2][1], size.GetWidth());

    // ┌─┐
    // │ │
    // └─┘
    Print(position, m_border[0][0], color);
    Print(position + Point(size.GetWidth() + 1, 0), m_border[0][2], color);
    Print(position + Point(0, size.GetHeight() + 1), m_border[2][0], color);
    Print(position + Point(size.GetWidth() + 1, size.GetHeight() + 1), m_border[2][2], color);

    if (!name.empty()) {
        Print(position + Point(2, 0), ' ', color);
        Print(name);
        Print(' ');
    }
}

Point Screen::GetCursorPosition() const {
    int x, y;
    getyx(stdscr, y, x);
    return {x, y};
}

const Point &Screen::GetCamera() const {
    return m_camera;
}

const Size &Screen::GetScreenSize() const {
    return m_area.GetSize();
}

void Screen::SetColor(Color color) const {
    attron(COLOR_PAIR(static_cast<short>(color)));
}

void Screen::PrintControls() {
    Move(m_camera + Point(1, m_area.GetSize().GetHeight()));

    // Print each control in case it's condition is met
    bool            didPrint = false;
    bool            first    = true;
    for (const auto &control: m_controls) {

        // Check the condition
        if (control.second()) {
            Print(first ? " " : " | ");
            didPrint = true;
            first    = false;

            // Truncate controls if they won't fit the screen
            if (m_area.GetSize().GetWidth() - GetCursorPosition().GetX() <= static_cast<int>(control.first.length())) {
                Print("...", m_defaultColor);
                break;
            }
            Print(control.first, m_defaultColor);
        }
    }

    // Print the last space in case we printed something
    if (didPrint) {
        Print(' ');
    }
}

void Screen::PrintPlayerHealth() {
    int precision = (
            m_area.GetSize().GetWidth() <= 22
            ? 6
            : (m_area.GetSize().GetWidth() <= 32 ? 10 : 20)
    );
    PrintProgressBar(
            m_camera + Point(m_area.GetSize().GetWidth() - 5 - precision, -2), "HP", m_defaultColor, Color::RedBlack,
            precision, 0, m_player->GetMaxHitpoints(), m_player->GetHitpoints()
    );
}

void Screen::PrintPlayerDirection() {
    auto position = m_camera + m_area.GetSize().ToPoint() - Point(2, 2);
    DrawBorder({position - Point(1, 1), {3, 3}}, "", m_defaultColor);

    for (auto y = 0; y < 3; ++y) {

        for (auto x = 0; x < 3; ++x) {
            Print(position + Point(x, y), ' ', Color::RedBlack);
        }
        Print(position + Point(1, 1), 'o', Color::RedBlack);
    }

    auto direction = m_player->GetDirection();

    Print(position + Point(1, 1) + Direction::ToPoint(direction), m_player->GetDirectionChar(),
          Color::RedBlack);
}

void Screen::PrintPlayerAction() {
    //DrawBorder({position - Point(1, 1), {3, 3}}, "", m_defaultColor);

    std::string prefix,
                suffix;

    if (m_player->IsBreaking()) {
        // Generate action label for breaking stuff
        prefix = "Breaking";
        if (m_player->IsTargetBlockBreakable() && m_player->CanBreakTargetBlockBlock()) {
            auto progress = static_cast<int>(m_player->GetBreakingProgress() * 100);
            suffix += 100 <= progress ? "" : (progress < 10 ? "  " : /* 10 <= progress && progress < 100 */ " ");
            suffix        = to_string(static_cast<int>(m_player->GetBreakingProgress() * 100)) + "%";
        } else if (m_player->IsTargetBlockBreakable()){
            // Block is breakable, but entity doesn't have required mining power
            prefix = "Need better tools for";
        } else{
            // Target block is not breakable
            prefix += " unbreakable";
        }
    } else {
        // Generic "looking at" label
        prefix = "Looking at";
    }

    // Get block and its name
    auto block = m_player->IsBreaking() ? m_player->GetBreakedBlock() : m_player->GetSelectedBlock();
    auto blockName  = block != nullptr ? block->ToString() : "Void";

    // Generate full action label
    auto fullAction = prefix + " " + blockName + " " + suffix;

    // Throw all this work in garbage and generate three dots in case the old description would not fit the screen
    if (m_area.GetSize().GetWidth() / 2 <= static_cast<int>(fullAction.size()) / 2) {
        fullAction = "...";
    }

    // Print centered description
    auto cameraShift = Point(static_cast<int>((m_area.GetSize().GetWidth() - fullAction.size()) / 2), 2);
    Print(m_camera + cameraShift, fullAction, m_defaultColor);
}

void Screen::PrintProgressBar(
        const Point &position, const std::string &name, Color borderColor, Color lineColor,
        int precision, int min, int max, int value
) {
    DrawBorder({position, {precision + 2, 1}}, name + ": " + std::to_string(value), borderColor);

    Move(position + Point(1, 1));

    Print(' ', borderColor);
    max -= min;
    value -= min;
    for (int i = 0; i < precision; ++i) {
        if ((double) max / precision * i < value) {
            Print(97 | A_ALTCHARSET, lineColor);
        } else {
            Print(' ', lineColor);
        }
    }
    Print(' ', borderColor);
}

void Screen::InitTerminal(bool createResizeHandler) {

    // Initiate ncurses (start window, initiate colors and so on
    initscr();
    start_color();
    cbreak();
    noecho();
    nodelay(stdscr, true);
    curs_set(FALSE);
    timeout(0);
    refresh();

    // Handle ncurses resize signal
    if (createResizeHandler) {
        ncursesLambdaResizeHandler = [&]() {
            TerminalResized();
        };
        signal(SIGWINCH, ncursesResizeHandler);
    }

    // Set size
    SetTerminalSize({COLS, LINES});

    // Initialize colors
    init_pair(static_cast<short>(Color::WhiteBlack), COLOR_WHITE, COLOR_BLACK);
    init_pair(static_cast<short>(Color::RedBlack), COLOR_RED, COLOR_BLACK);
    init_pair(static_cast<short>(Color::GreenYellow), COLOR_GREEN, COLOR_YELLOW);
    init_pair(static_cast<short>(Color::BlueWhite), COLOR_BLUE, COLOR_WHITE);
    init_pair(static_cast<short>(Color::CyanMagenta), COLOR_CYAN, COLOR_MAGENTA);
    init_pair(static_cast<short>(Color::WhiteMagenta), COLOR_WHITE, COLOR_MAGENTA);
    init_pair(static_cast<short>(Color::MagentaWhite), COLOR_MAGENTA, COLOR_WHITE);
    init_pair(static_cast<short>(Color::GreenWhite), COLOR_GREEN, COLOR_WHITE);
}

void Screen::EndTerminal(bool removeResizeHandler) {

    // End ncurses window
    endwin();

    // Remove resize handler
    if (removeResizeHandler) {
        signal(SIGWINCH, SIG_DFL);
    }
}

void Screen::TerminalResized() {
    m_terminalResized = true;
}

const uint32_t Screen::m_border[3][3] = {
        {108 | A_ALTCHARSET, ACS_HLINE, 107 | A_ALTCHARSET},
        {ACS_VLINE, 0, ACS_VLINE},
        {109 | A_ALTCHARSET, ACS_HLINE, 106 | A_ALTCHARSET},
};

void ncursesResizeHandler(int signal) {

    // No other signal should reach this ugly function
    assert(signal == SIGWINCH);
    ncursesLambdaResizeHandler();
}
