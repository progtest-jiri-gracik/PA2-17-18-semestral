//
// Created by Jiří Gracík on 16/05/2018.
//

#include <ncurses.h>
#include <iostream>
#include "ChunkManager.hpp"
#include "to_string.hpp"

// PUBLIC

ChunkManager::ChunkManager(const Size &chunkSize, std::vector<chunk_t> &chunks)
        : m_chunkSize(chunkSize),
          m_chunks(std::move(chunks)),
          m_mapSize(
                  static_cast<int>(chunkSize.GetWidth() * m_chunks.size()),
                  chunkSize.GetHeight()
          ) {}

void ChunkManager::Update(GameState &state) {
    if (state != GameState::Paused && state != GameState::TooSmallToRun) {
        for (auto &chunk : m_chunks) {
            for (auto &block : chunk) {
                block->Update(*this);
            }
        }
    }
}

void ChunkManager::Draw(IScreenDriver &screen) {
    auto topLeftCorner     = screen.GetCamera();
    auto bottomRightCorner = topLeftCorner + screen.GetScreenSize().ToPoint();

    // Last chunk index must be smaller than chunk count
    auto lastChunkIndex = std::min(bottomRightCorner.GetX() / m_chunkSize.GetWidth(),
                                   static_cast<int>(m_chunks.size() - 1));

    // First chunk index should be smaller or equal to last chunk index
    auto firstChunkIndex = std::min(topLeftCorner.GetX() / m_chunkSize.GetWidth(), lastChunkIndex);

    // Both indexes should be non-negative
    lastChunkIndex  = std::max(lastChunkIndex, 0);
    firstChunkIndex = std::max(firstChunkIndex, 0);

    // There is nothing to draw (map is too high/low to fit the screen)
    if (topLeftCorner.GetY() >= m_chunkSize.GetHeight() || bottomRightCorner.GetY() < 0) {
        return;
    }

    auto blockBeginIndex = std::max(topLeftCorner.GetY() * m_chunkSize.GetWidth(), 0);
    auto blockEndIndex   = std::min((bottomRightCorner.GetY()) * m_chunkSize.GetWidth(), m_chunkSize.GetArea());

    for (auto i = firstChunkIndex; i <= lastChunkIndex; ++i) {

        const auto &chunk = m_chunks[i];

        // Prepare begin and end iterators
        auto       blockIterator     = chunk.begin() + blockBeginIndex;
        const auto &blockEndIterator = blockEndIndex < static_cast<int>(chunk.size())
                                       ? chunk.begin() + blockEndIndex
                                       : chunk.end();

        for (; blockIterator != blockEndIterator; blockIterator++) {
            auto block = blockIterator->get();

            // Block must exist
            if (block == nullptr) {
                continue;
            }

            // Block must fit the screen
            auto blockPositionX = block->GetPosition().GetX();
            if (block != nullptr &&
                topLeftCorner.GetX() <= blockPositionX &&
                blockPositionX < bottomRightCorner.GetX()) {

                // Draw the block
                block->Draw(screen);
            }
        }
    }
}

Block *ChunkManager::GetBlock(const Point &position) {
    if (!IsValidPosition(position)) {
        return nullptr;
    }

    auto chunkIndex = GetChunkIndex(position),
         blockIndex = position.GetY() * m_chunkSize.GetWidth() +
                      (position.GetX() % m_chunkSize.GetWidth());
    return m_chunks[chunkIndex][blockIndex].get();
}

bool ChunkManager::IsValidPosition(const Point &position) const {
    return 0 <= position.GetX() && position.GetX() < GetMapSize().GetWidth() &&
           0 <= position.GetY() && position.GetY() < GetMapSize().GetHeight();
}

void ChunkManager::SetBlock(Block *block) {
    auto position = block->GetPosition();
    if (!IsValidPosition(position)) {
        throw std::invalid_argument(
                to_string(position) +
                " is not a valid position for " +
                to_string(m_chunks.size()) +
                " chunks of " +
                to_string(m_chunkSize)
        );
    }

    auto chunkIndex = GetChunkIndex(position),
         blockIndex = position.GetY() * m_chunkSize.GetWidth() +
                      (position.GetX() % m_chunkSize.GetWidth());
    m_chunks[chunkIndex].at(blockIndex) = std::unique_ptr<Block>(block);
}

Block *ChunkManager::operator[](const Point &position) {
    return GetBlock(position);
}

// PRIVATE

int ChunkManager::GetChunkIndex(const Point &position) const {
    auto chunkIndex = position.GetX() / m_chunkSize.GetWidth();

    if (position.GetX() < 0 || chunkIndex < 0 || static_cast<int>(m_chunks.size()) <= chunkIndex) {
        throw std::invalid_argument(
                to_string(position) +
                " is not a valid position for " +
                to_string(m_chunks.size()) +
                " chunks of " +
                to_string(m_chunkSize)
        );
    }
    return chunkIndex;
}
