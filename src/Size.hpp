//
// Created by Jiří Gracík on 13/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_SIZE_HPP
#define PROGTEST_17_18_SEMESTRAL_SIZE_HPP

#include <memory>
#include <ostream>
#include "Point.hpp"

class Point;

class Size {

public:

    /**
     * Create an empty size of {0, 0}
     */
    Size();

    /**
     * Create size from point (x becomes width and y becomes height)
     * @param point
     */
    explicit Size(const Point &point);

    /**
     * Create size from width and height pair
     * @param pair
     */
    explicit Size(std::pair<int, int> pair);

    /**
     * Create size from width and height
     * @param width
     * @param height
     */
    Size(int width, int height);

    /**
     * Get size width
     * @return
     */
    int GetWidth() const;

    /**
     * Get size height
     * @return
     */
    int GetHeight() const;

    /**
     * Get rectangle area (width * height)
     * @return
     */
    int GetArea() const;

    /**
     * Convert size to point (width becomes x and height becomes y)
     * @return
     */
    Point ToPoint() const;

    Size &operator+=(const Size &size);

    Size &operator-=(const Size &size);

    Size operator+(const Size &size) const;

    Size operator-(const Size &size) const;

    Size operator-() const;

    Size operator*(int a) const;

    Size operator/(int a) const;

    /**
     * Both dimensions are equal
     * @param lhs
     * @param rhs
     * @return
     */
    friend bool operator==(const Size &lhs, const Size &rhs);

    /**
     * Dimensions differ
     * @param lhs
     * @param rhs
     * @return
     */
    friend bool operator!=(const Size &lhs, const Size &rhs);

    /**
     * Area is smaller
     * @param lhs
     * @param rhs
     * @return
     */
    friend bool operator<(const Size &lhs, const Size &rhs);

    /**
     * Area is bigger
     * @param lhs
     * @param rhs
     * @return
     */
    friend bool operator>(const Size &lhs, const Size &rhs);

    /**
     * Area is smaller or equal
     * @param lhs
     * @param rhs
     * @return
     */
    friend bool operator<=(const Size &lhs, const Size &rhs);

    /**
     * Area is bigger or equal
     * @param lhs
     * @param rhs
     * @return
     */
    friend bool operator>=(const Size &lhs, const Size &rhs);

    friend std::ostream &operator<<(std::ostream &os, const Size &size);

protected:
    std::shared_ptr<Point> m_point;
};


#endif //PROGTEST_17_18_SEMESTRAL_SIZE_HPP
