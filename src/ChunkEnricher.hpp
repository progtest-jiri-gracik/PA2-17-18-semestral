//
// Created by Jiří Gracík on 18/05/2018.
//

#ifndef PROGTEST_17_18_SEMESTRAL_CHUNKENRICHER_HPP
#define PROGTEST_17_18_SEMESTRAL_CHUNKENRICHER_HPP


#include <functional>
#include "ChunkManager.hpp"

/**
 * Enrich a map managed by chunk manager with some metals/minerals/whatever we want
 */
class ChunkEnricher {

public:

    /**
     * Setup all generation parameters
     * @param maxLevel
     * @param minLevel
     * @param minCountPerChunk
     * @param maxCountPerChunk
     * @param minCountPerVein
     * @param maxCountPerVein
     * @param blockGenerator
     */
    ChunkEnricher(double maxLevel, double minLevel, int minCountPerChunk, int maxCountPerChunk, int minCountPerVein,
                  int maxCountPerVein, const std::function<Block *(const Point &)> &blockGenerator);

    /**
     * Generate with
     * @param chunkManager
     */
    void Enrich(ChunkManager &chunkManager);

private:

    // Generation settings
    double                                        m_maxLevel, m_minLevel;
    int                                           m_minCountPerChunk, m_maxCountPerChunk,
                                                  m_minCountPerVein, m_maxCountPerVein;

    // Block generator
    std::function<Block *(const Point &position)> m_blockGenerator;
};

#endif //PROGTEST_17_18_SEMESTRAL_CHUNKENRICHER_HPP
